<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package climatenow
 */

get_header();
?>

	<main id="primary" class="site-main">
		<div class="section404">
			<div class="container--single">
				<div class="col-smD-6 no-padding">
					<h1 class="single-title">404: Page Cannot be found</h1>
				</div>
			</div>
		</div>

	</main><!-- #main -->

<?php
get_footer();

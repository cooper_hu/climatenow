<?php
/**
 * climatenow functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package climatenow
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '2.6.7' );
}

if ( ! function_exists( 'climatenow_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function climatenow_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on climatenow, use a find and replace
		 * to change 'climatenow' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'climatenow', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

    // Custom image Sizes
    add_image_size( 'portrait', 400, 500, true );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'climatenow' ),
        'header-right'  => __( 'Top Navigation', 'text_domain' ),

        'footer-menu'  => __( 'Footer Menu', 'text_domain' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'climatenow_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function climatenow_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'climatenow_content_width', 1200 );
}
add_action( 'after_setup_theme', 'climatenow_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function climatenow_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'climatenow' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'climatenow' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'climatenow_widgets_init' );

/**
 * Enqueue scripts and styles.
 */


function climatenow_scripts() {
	wp_enqueue_style( 'climatenow-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'climatenow-customstyle', get_template_directory_uri() . '/assets/css/main.css', array(), _S_VERSION );

	wp_style_add_data( 'climatenow-style', 'rtl', 'replace' );

	wp_enqueue_script( 'climatenow-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
    
    wp_deregister_script('jquery');
    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

    // Load AJAX
    wp_enqueue_script( 'ajax', get_stylesheet_directory_uri(). '/assets/js/js-filtering.js', array('jquery'), _S_VERSION, true );
    wp_localize_script( 'ajax', 'wp_ajax',
        array('ajax_url' => admin_url('admin-ajax.php'))
    );

	wp_enqueue_script( 'climatenow-customscript', get_template_directory_uri() . '/assets/js/js-bundle.js', array('jquery'), _S_VERSION, true);
}
add_action( 'wp_enqueue_scripts', 'climatenow_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-cpt.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Create custom toolbars
function my_toolbars( $toolbars ) {
	$toolbars['Ultra Slim' ] = array();
	$toolbars['Ultra Slim' ][1] = array('bold' , 'italic', 'underline', 'link', 'undo', 'redo', 'removeformat', 'fullscreen' );

	$toolbars['B/I' ] = array();
	$toolbars['B/I' ][1] = array('bold' , 'italic', 'undo', 'redo', 'removeformat', 'fullscreen' );

  $toolbars['Base' ] = array();
  $toolbars['Base' ][1] = array('formatselect', 'bold' , 'italic', 'underline', 'link', 'undo', 'redo', 'removeformat', 'fullscreen' );

  $toolbars['Events' ] = array();
  $toolbars['Events' ][1] = array('formatselect', 'bold' , 'italic', 'underline', 'link', 'bullist', 'numlist', 'undo', 'redo', 'removeformat', 'fullscreen' );

    return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );

// Set up Options Page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Sitewide Settings',
		'menu_title'	=> 'Sitewide Settings',
		'menu_slug' 	=> 'sitewide-settings',
		'capability'	=> 'edit_posts',
		// 'redirect'		=> false
	));
}

// Move Yoast to bottom
add_filter( 'wpseo_metabox_prio', function() { return 'low'; } );

/* ***************************************************
 * Add custom column header
 * *************************************************** */
function change_post_columns ( $columns ) {
    unset($columns['date']);
    unset($columns['categories']);
    unset($columns['tags']);
    unset($columns['comments']);
    unset($columns['taxonomy-series']);
    unset($columns['taxonomy-person']);

    $columns['taxonomy-series'] = __( 'Series', 'my-text-domain' );
    $columns['acf_field'] = __( 'Episode', 'my-text-domain' );
   	
   	return array_merge ( $columns, array ( 
     	'date' => __('Date')
   ) );

}
add_filter ( 'manage_post_posts_columns', 'change_post_columns' );

function add_custom_columns ( $columns ) {
    unset($columns['date']);
    unset( $columns['taxonomy-series'] );
    unset( $columns['taxonomy-person'] );

    // $columns['taxonomy-series'] = __( 'Series', 'my-text-domain' );
    $columns['acf_field'] = __( 'Episode', 'my-text-domain' );
   	
   	return array_merge ( $columns, array ( 
     	'date' => __('Date')
   ) );

}

// add_filter ( 'manage_video_posts_columns', 'add_custom_columns' );
add_filter ( 'manage_podcast_posts_columns', 'add_custom_columns' );

function add_custom_video_columns ( $columns ) {
    unset($columns['date']);
    unset( $columns['taxonomy-series'] );
    unset( $columns['taxonomy-person'] );

    $columns['taxonomy-video-series'] = __( 'Series', 'my-text-domain' );
    $columns['acf_field'] = __( 'Episode', 'my-text-domain' );
    
    return array_merge ( $columns, array ( 
        'date' => __('Date')
   ) );
}

add_filter ( 'manage_video_posts_columns', 'add_custom_video_columns' );


function add_custom_article_columns ( $columns ) {
    unset( $columns['date'] );
    
    unset( $columns['taxonomy-person'] );

    $columns['taxonomy-article-series'] = __( 'Series', 'my-text-domain' );
    $columns['acf_field'] = __( 'Episode', 'my-text-domain' );
    
    return array_merge ( $columns, array ( 
        'date' => __('Date')
   ) );
}

add_filter ( 'manage_article_posts_columns', 'add_custom_article_columns' );

/* ***************************************************
 * Setup custom column info
 * *************************************************** */
function custom_column_setup( $column, $post_id ) {
  switch ( $column ) {
    // display the value of an ACF (Advanced Custom Fields) field
    case 'acf_field' :
      echo get_field( 'episode', $post_id );  
      break;
  }
}

add_action( 'manage_video_posts_custom_column' , 'custom_column_setup', 10, 2 );
add_action( 'manage_podcast_posts_custom_column' , 'custom_column_setup', 10, 2 );
add_action( 'manage_article_posts_custom_column' , 'custom_column_setup', 10, 2 );

/* ***************************************************
 * Setup custom column info
 * *************************************************** */
function set_custom_sortable_columns( $columns ) {
  $columns['acf_field'] = 'acf_field';

  return $columns;
}

add_filter( 'manage_edit-video_sortable_columns', 'set_custom_sortable_columns' );
add_filter( 'manage_edit-podcast_sortable_columns', 'set_custom_sortable_columns' );
add_filter( 'manage_edit-article_sortable_columns', 'set_custom_sortable_columns' );


/* ***************************************************
 * 
 * *************************************************** */
function column_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'acf_field' == $orderby ) {
    $query->set( 'meta_key', 'episode' );
    $query->set( 'orderby', 'meta_value_num' );
  }
}

add_action( 'pre_get_posts', 'column_custom_orderby' );


function post_thumbnail_add_label($content, $post_id, $thumbnail_id) {
    $post = get_post($post_id);
    if ($post->post_type == 'post' || $post->post_type == 'podcast' || $post->post_type == 'video') {
        $content .= '<div class="acf-label"><p class="description">(Upload at 16:9 min width 1200px).</p></div>';
        return $content;
    }

    return $content;
}
add_filter('admin_post_thumbnail_html', 'post_thumbnail_add_label', 10, 3);

// Hide default post from admin
/*
function remove_menus(){
  remove_menu_page( 'edit.php' ); // Posts
}
add_action( 'admin_menu', 'remove_menus' );*/


// Ajax Callback
add_action('wp_ajax_nopriv_propertyfilter', 'search_terms');
add_action('wp_ajax_propertyfilter', 'search_terms');

function search_terms() {
    if (get_field('hide-articles', 'options') != true) {
      $args = array( 
          'post_type'     => array('video', 'podcast', 'article'), 
          'posts_per_page' => -1,
          'meta_query' => array(
              'relation' => 'OR',
              array(
                'key' => 'coming_soon',
                'value' => '0',
                'compare' => 'NOT EXISTS'
              ),
              array(
                'key' => 'coming_soon',
                'value' => '0',
                'compare' => '=='
              )
          )
      );
    } else {
      $args = array( 
          'post_type'     => array('video', 'podcast'), 
          'posts_per_page' => -1,
          'meta_query' => array(
              'relation' => 'OR',
              array(
                'key' => 'coming_soon',
                'value' => '0',
                'compare' => 'NOT EXISTS'
              ),
              array(
                'key' => 'coming_soon',
                'value' => '0',
                'compare' => '=='
              )
          )
      );
    }
    if (isset($_POST['search'])) {
        $args['s'] = $_POST['search'];
    }

    // The Query
    $the_query = new WP_Query(); 
    $the_query->parse_query( $args );
    relevanssi_do_query( $the_query );


    if ($the_query->have_posts()) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            get_template_part( 'template-parts/search', 'modal-item' );
        }
    } else {
        // Echo 'NOPOST' so Javascript can show a different view (JS needs to do this so it can also hide the Load More button (: )
        echo 'NOPOST';
    }
    wp_reset_postdata();

    die();
}

// Hide Person/Host default description field column
function remove_default_tax_desc($columns) {
  if( isset( $columns['description'] ) )
    unset( $columns['description'] );   
    return $columns;
}

add_filter('manage_edit-person_columns', 'remove_default_tax_desc');
add_filter('manage_edit-host_columns', 'remove_default_tax_desc');

// Hide Person/Host default description field edit/add
function hide_description_row() {
    echo "<style> .term-description-wrap { display:none; } </style>";
}

add_action( 'person_edit_form', 'hide_description_row');
add_action( 'person_add_form', 'hide_description_row');

add_action( 'host_edit_form', 'hide_description_row');
add_action( 'host_add_form', 'hide_description_row');

// Hide post/media/user from new wp admin bar
add_action( 'wp_before_admin_bar_render', 'wpse20131211_admin_bar' );

function wpse20131211_admin_bar() {
   global $wp_admin_bar;
   $wp_admin_bar->remove_menu('wp-logo');
   $wp_admin_bar->remove_menu('comments');
   $wp_admin_bar->remove_node( 'new-post' );
   $wp_admin_bar->remove_node( 'new-media' );
   $wp_admin_bar->remove_node( 'new-page' );
   $wp_admin_bar->remove_node( 'new-user' );
   $wp_admin_bar->remove_node( 'updates' );
}

// remove default post
function post_remove () {  //creating functions post_remove for removing menu item 
   remove_menu_page('edit.php');
}

add_action('admin_menu', 'post_remove');  



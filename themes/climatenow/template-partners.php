<?php
/**
 * Template Name: Partners
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package climatenow
 */

get_header();

?>

<main id="primary" class="single-page-wrapper">
    <div class="container--partners">
        <div class="archive-section">
            <div class="partner-section-intro">
                <!--<h2 class="single-title--sm no-margin-bottom">Series 01</h2>-->
                <h1 class="single-title thick no-margin-top"><?= get_field('headline'); ?></h1>
                <?= get_field('description'); ?>
            </div>
            <?php if (get_field('partner_info')) : 
                $partner = get_field('partner_info'); ?>
                <div class="partner-section">
                    <?php if ($partner['logo']) : ?>
                        <div class="partner-section__left">
                            <img src="<?= $partner['logo']['url']; ?>" alt="<?= $partner['logo']['alt']; ?>" width="<?= $partner['logo']['width']; ?>" height="<?= $partner['logo']['height']; ?>"/>
                        </div>
                    <?php endif; ?>
                    <div class="partner-section__right">
                        <?= $partner['about']; ?>
                    </div>
                </div>
            <?php endif; ?>
       
            <?php if (have_rows('partner_items')) : ?>
                <div class="partner-item-wrapper">
                    <?php while( have_rows('partner_items') ) : the_row();
                        $img = get_sub_field('partner_image');
                        $content = get_sub_field('partner_content');
                        $type = $content['type'];
                        $link = $content['link']; ?>
                        <div class="partner-item">
                            <div class="partner-item__left">
                                <?php if ($type === 'link') : ?>
                                    <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>">
                                        <img src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>"/>
                                    </a>
                                <?php else : ?>
                                    <img src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>"/>
                                <?php endif; ?>
                            </div>
                            <div class="partner-item__right">
                                <div>
                                    <h2 class="partner-item-headline"><?= $content['headline']; ?></h2>
                                    <?= $content['description']; ?>

                                    <?php if ($type === 'popup') : ?>
                                        <a href="#partnerPopup" class="btn js-show-form">Connect With Us</a>
                                    <?php elseif ($type === 'link') : ?>
                                        <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>" class="btn"><?= $link['title']; ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

            <?php if (have_rows('g-logos')) : ?>
                <?php if (get_field('grid_headline')): ?>
                    <h2 class="single-title no-margin-top"><?= get_field('grid_headline'); ?></h2>
                <?php endif; ?>

                <div class="g-logo-wrapper">
                    <?php while( have_rows('g-logos') ) : the_row();
                        $img = get_sub_field('g-logo'); ?>
                        <div class="g-logo-item">
                            <?php if (get_sub_field('url')) : ?>
                                <a href="<?= get_sub_field('url'); ?>" target="_blank">
                                    <img src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>"/>
                                </a>
                            <?php else : ?>
                                <img src="<?= $img['url']; ?>" alt="<?= $img['alt']; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>"/>
                            <?php endif; ?>
                            
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

            <div class="partners-misc">
                <h3 class="single-title thick no-margin-top"><?= get_field('misc_headline'); ?></h3>
                <?= get_field('misc_description'); ?>
            </div>
        </div>
    </div>
</main>

<div id="partnerPopup" class="partner-form-popup mfp-hide">
    <h2 class="single-title no-margin-top">Connect With Us</h2>
    <!--[if lte IE 8]>
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
    <![endif]-->
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
    <script>
      hbspt.forms.create({
        region: "na1",
        portalId: "21163325",
        formId: "8dc63395-5b54-45b1-bb2f-09811c1e3d01"
    });
    </script>
</div>

<?php
get_footer();

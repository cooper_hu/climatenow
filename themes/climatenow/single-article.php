<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package climatenow
 */

get_header(); ?>

<main id="primary" class="single-page-wrapper">
    <article>
        <?php get_template_part('template-parts/single-header'); ?>
        <div class="single-content">
            <div class="container--single">

                <div class="col-smD-4">
                    <?php get_template_part('template-parts/section-author'); ?>
                    <?php get_template_part('template-parts/section-people'); ?>
                    <?php get_template_part('template-parts/section-related-media'); ?>
                </div>

                <?php // get_template_part('template-parts/single-transcript'); ?>
                <section class="single-transcript">
                    <div class="single-transcript__inner">
                        <div class="content-wrapper" id="pageContent">
                            <div class="transcript-item">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="single-transcript__footer">
                        <div class="transcript-item">
                            <div class="single-transcript__footer--flex">
                                <div class="left">
                                    <a href="<?php the_field('all-content-page','options'); ?>" class="btn" id="readMoreBtn">Read More</a>
                                </div>
                                <div class="right">
                                    <a href="<?php the_field('all-content-page','options'); ?>" class="btn accent--left animate">All Content</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </article>
</main><!-- #main -->

<?php get_template_part('template-parts/share-modal'); ?>

<?php
get_footer();

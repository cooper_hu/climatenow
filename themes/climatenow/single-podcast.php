<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package climatenow
 */

get_header(); 
$seriesInfo    = get_field('series');
$episodeNumber = get_field('episode');
$contentGroup  = get_field('info');
?>

<main id="primary" class="single-page-wrapper">
    <article>
        <header class="single-podcast-header">
            <div class="container--single">
                <div class="col-smD-4">
                    <div class="single-tag">Podcast Episode <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?>
                        <a href="#shareModal" class="single-share">
                            <i class="fas fa-share-alt"></i>
                        </a>
                    </div>
                
                    <h1 class="single-title <?php if ($contentGroup['subheadline']) echo 'margin-bottom-sm'; ?>"><?php the_title(); ?></h1>

                    <?php if ($contentGroup['subheadline']) : ?>
                        <h2 class="single-title--sm" style="margin-top: 0;"><?= $contentGroup['subheadline']; ?></h2>
                    <?php endif; ?>
                </div>

                <div class="col-smD-flex">
                    <div class="col-smD-4">
                        
                        <div class="single-description">
                            <?= $contentGroup['description']; ?>
                        </div>

                        <div class="single-podcast-header-break"></div>

                        <?php get_template_part('template-parts/section-people'); ?>
                        <?php get_template_part('template-parts/section-host'); ?>
                    </div>
                    <div class="col-smD-6">
                        <?php get_template_part('template-parts/media-podcast'); ?>

                        <div class="single-related-wrapper">
                            <?php get_template_part('template-parts/section-related-media'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container--transcript">
            <div id="transcript" class="single-video-transcript-anchor"></div>
            <?php get_template_part('template-parts/single-transcript'); ?>
        </div>
    </article>
</main><!-- #main -->

<?php get_template_part('template-parts/share-modal'); ?>

<?php
get_footer();

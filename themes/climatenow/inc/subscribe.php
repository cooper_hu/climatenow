<?php

    /*******************************************************
    *
    *   MailChimp subscribe function
    *
    *******************************************************/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
global $mc_ola;

// $list_id = '4a6440e80c';
// $authToken = 'c30a1210ef2c2e1722df7ab9f622c891-us7';
// $server = 'us7';

$list_id = '16169a9b2f';
$authToken = 'd56993a70b97a7705abc27fc0c20bfa0-us1';
$server = 'us1';

// The data to send to the API

$postData = array(
    "email_address" => $_POST["newsletter_email"], 
    "status" => "subscribed", 
    "tags" => array("climatenow.com subscribers")
);

// Setup cURL
$ch = curl_init('https://'.$server.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Authorization: apikey '.$authToken,
        'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postData)
));
// Send the request
$response = curl_exec($ch);

curl_close ($ch);

echo $response; ?>
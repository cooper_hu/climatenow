<?php
// Register Custom Taxonomy
function person_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Persons', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Person', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Person', 'text_domain' ),
        'all_items'                  => __( 'All Persons', 'text_domain' ),
        'parent_item'                => __( 'Parent Person', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Person:', 'text_domain' ),
        'new_item_name'              => __( 'New Person Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Person', 'text_domain' ),
        'edit_item'                  => __( 'Edit Person', 'text_domain' ),
        'update_item'                => __( 'Update Person', 'text_domain' ),
        'view_item'                  => __( 'View Person', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate persons with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove persons', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Persons', 'text_domain' ),
        'search_items'               => __( 'Search Persons', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No persons', 'text_domain' ),
        'items_list'                 => __( 'Persons list', 'text_domain' ),
        'items_list_navigation'      => __( 'Persons list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'meta_box_cb'                => false,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'person', array( 'post', ' video', ' podcast' ), $args );
}
add_action( 'init', 'person_taxonomy', 0 );

// Register Custom Taxonomy
function host_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Hosts', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Host', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Host', 'text_domain' ),
        'all_items'                  => __( 'All Hosts', 'text_domain' ),
        'parent_item'                => __( 'Parent Host', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Host:', 'text_domain' ),
        'new_item_name'              => __( 'New Host Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Host', 'text_domain' ),
        'edit_item'                  => __( 'Edit Host', 'text_domain' ),
        'update_item'                => __( 'Update Host', 'text_domain' ),
        'view_item'                  => __( 'View Host', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate hosts with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove hosts', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Hosts', 'text_domain' ),
        'search_items'               => __( 'Search Hosts', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No hosts', 'text_domain' ),
        'items_list'                 => __( 'Hosts list', 'text_domain' ),
        'items_list_navigation'      => __( 'Hosts list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'meta_box_cb'                => false,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'host', array( 'post', ' video', ' podcast' ), $args );
}
add_action( 'init', 'host_taxonomy', 0 );

// Register Custom Post Type
function video_cpt() {

    $labels = array(
        'name'                  => _x( 'Videos', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Videos', 'text_domain' ),
        'name_admin_bar'        => __( 'Video', 'text_domain' ),
        'archives'              => __( 'Video Archives', 'text_domain' ),
        'attributes'            => __( 'Video Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Video:', 'text_domain' ),
        'all_items'             => __( 'All Videos', 'text_domain' ),
        'add_new_item'          => __( 'Add New Video', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Video', 'text_domain' ),
        'edit_item'             => __( 'Edit Video', 'text_domain' ),
        'update_item'           => __( 'Update Video', 'text_domain' ),
        'view_item'             => __( 'View Video', 'text_domain' ),
        'view_items'            => __( 'View Videos', 'text_domain' ),
        'search_items'          => __( 'Search Video', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into video', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this video', 'text_domain' ),
        'items_list'            => __( 'Videos list', 'text_domain' ),
        'items_list_navigation' => __( 'Videos list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter videos list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Video', 'text_domain' ),
        'description'           => __( 'Video Post Type', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'            => array( 'person', 'video-series', 'category', 'host' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-video',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'video', $args );

}
add_action( 'init', 'video_cpt', 0 );

function video_series_taxonomy() {
    $labels = array(
        'name'                       => _x( 'Video Series', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Video Series', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Video Series', 'text_domain' ),
        'all_items'                  => __( 'All Series', 'text_domain' ),
        'parent_item'                => __( 'Parent Series', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Series:', 'text_domain' ),
        'new_item_name'              => __( 'New Series Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Series', 'text_domain' ),
        'edit_item'                  => __( 'Edit Series', 'text_domain' ),
        'update_item'                => __( 'Update Series', 'text_domain' ),
        'view_item'                  => __( 'View Series', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate series with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove series', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Series', 'text_domain' ),
        'search_items'               => __( 'Search Series', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No persons', 'text_domain' ),
        'items_list'                 => __( 'Series list', 'text_domain' ),
        'items_list_navigation'      => __( 'Series list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'meta_box_cb'                => false,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'video-series', array( 'video' ), $args );
}

add_action( 'init', 'video_series_taxonomy', 0 );

function podcast_cpt() {

    $labels = array(
        'name'                  => _x( 'Podcasts', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Podcast', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Podcast', 'text_domain' ),
        'name_admin_bar'        => __( 'Podcasts', 'text_domain' ),
        'archives'              => __( 'Podcast Archives', 'text_domain' ),
        'attributes'            => __( 'Podcast Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Podcast:', 'text_domain' ),
        'all_items'             => __( 'All Podcasts', 'text_domain' ),
        'add_new_item'          => __( 'Add New Podcast', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Podcast', 'text_domain' ),
        'edit_item'             => __( 'Edit Podcast', 'text_domain' ),
        'update_item'           => __( 'Update Podcast', 'text_domain' ),
        'view_item'             => __( 'View Podcast', 'text_domain' ),
        'view_items'            => __( 'View Podcasts', 'text_domain' ),
        'search_items'          => __( 'Search Podcast', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into podcast', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this podcast', 'text_domain' ),
        'items_list'            => __( 'Podcasts list', 'text_domain' ),
        'items_list_navigation' => __( 'Podcasts list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter podcasts list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Podcast', 'text_domain' ),
        'description'           => __( 'Podcast Post Type', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'            => array( 'person', 'podcast-seasons', 'category', 'host' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-audio',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'podcast', $args );

}
add_action( 'init', 'podcast_cpt', 0 );

function podcast_seasons_taxonomy() {
    $labels = array(
        'name'                       => _x( 'Podcast Seasons', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Podcast Seasons', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Podcast Series', 'text_domain' ),
        'all_items'                  => __( 'All Podcast', 'text_domain' ),
        'parent_item'                => __( 'Parent Season', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Season:', 'text_domain' ),
        'new_item_name'              => __( 'New Season Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Season', 'text_domain' ),
        'edit_item'                  => __( 'Edit Season', 'text_domain' ),
        'update_item'                => __( 'Update Season', 'text_domain' ),
        'view_item'                  => __( 'View Season', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate seasons with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove season', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Seasons', 'text_domain' ),
        'search_items'               => __( 'Search Seasons', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No seasons', 'text_domain' ),
        'items_list'                 => __( 'Seasons list', 'text_domain' ),
        'items_list_navigation'      => __( 'Seasons list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'meta_box_cb'                => false,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'podcast-seasons', array( 'podcast' ), $args );

}

add_action( 'init', 'podcast_seasons_taxonomy', 0 );



// Articles
// Register Custom Post Type
function article_cpt() {

    $labels = array(
        'name'                  => _x( 'Articles', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Article', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Articles', 'text_domain' ),
        'name_admin_bar'        => __( 'Article', 'text_domain' ),
        'archives'              => __( 'Article Archives', 'text_domain' ),
        'attributes'            => __( 'Article Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Article:', 'text_domain' ),
        'all_items'             => __( 'All Articles', 'text_domain' ),
        'add_new_item'          => __( 'Add New Article', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Article', 'text_domain' ),
        'edit_item'             => __( 'Edit Article', 'text_domain' ),
        'update_item'           => __( 'Update Article', 'text_domain' ),
        'view_item'             => __( 'View Article', 'text_domain' ),
        'view_items'            => __( 'View Articles', 'text_domain' ),
        'search_items'          => __( 'Search Article', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into article', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this article', 'text_domain' ),
        'items_list'            => __( 'Articles list', 'text_domain' ),
        'items_list_navigation' => __( 'Articles list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter articles list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Article', 'text_domain' ),
        'description'           => __( 'Article Post Type', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'            => array( 'person', 'article-series', 'category' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-aside',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'article', $args );

}
add_action( 'init', 'article_cpt', 0 );

// Add author support
function add_author_support_to_posts() {
   add_post_type_support( 'article', 'author' ); 
}
add_action( 'init', 'add_author_support_to_posts' );


function article_series_taxonomy() {
    $labels = array(
        'name'                       => _x( 'Article Series', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Article Series', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Article Series', 'text_domain' ),
        'all_items'                  => __( 'All Series', 'text_domain' ),
        'parent_item'                => __( 'Parent Series', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Series:', 'text_domain' ),
        'new_item_name'              => __( 'New Series Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Series', 'text_domain' ),
        'edit_item'                  => __( 'Edit Series', 'text_domain' ),
        'update_item'                => __( 'Update Series', 'text_domain' ),
        'view_item'                  => __( 'View Series', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate series with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove series', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Series', 'text_domain' ),
        'search_items'               => __( 'Search Series', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No persons', 'text_domain' ),
        'items_list'                 => __( 'Series list', 'text_domain' ),
        'items_list_navigation'      => __( 'Series list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'meta_box_cb'                => false,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'article-series', array( 'article' ), $args );
}

add_action( 'init', 'article_series_taxonomy', 0 );
<?php
/**
 * Template Name: Contact
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package climatenow
 */

get_header();
?>



<main id="primary" class="single-page-wrapper">
    <div class="container--single">
        <div class="contact">
            <div class="col-smD-6 no-padding">
                <h1 class="single-title"><?php the_field('headline'); ?></h1>
                <?php the_field('text'); ?>
                <?php echo do_shortcode(get_field('form')); ?>

                <div class="contact-socials">
                    <?php $social = get_field('social_links', 'options'); 
                          $podcast_links = get_field('podcast_links', 'options'); ?>
                    <ul>
                        <?php if ($social['youtube']) : ?>
                            <li>
                                <a href="<?= $social['youtube']; ?>" target="_blank">
                                    <span class="icon">
                                        <i class="fab fa-youtube"></i>
                                    </span>
                                    <span class="text">Youtube</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ($social['instagram']) : ?>
                            <li>
                                <a href="<?= $social['instagram']; ?>" target="_blank">
                                    <span class="icon">
                                        <i class="fab fa-instagram"></i>
                                    </span>
                                    <span class="text">Instagram</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ($social['linkedin']) : ?>
                            <li>
                                <a href="<?= $social['linkedin']; ?>" target="_blank">
                                    <span class="icon">
                                        <i class="fab fa-linkedin-in"></i>
                                    </span>
                                    <span class="text">Linkedin</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ($social['twitter']) : ?>
                            <li>
                                <a href="<?= $social['twitter']; ?>" target="_blank">
                                    <span class="icon">
                                        <i class="fab fa-twitter"></i>
                                    </span>
                                    <span class="text">Twitter</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($podcast_links['pocketcast']) : ?>
                            <li>
                                <a href="<?= $podcast_links['pocketcast']; ?>" target="_blank" class="pocketcast">
                                    <span class="icon">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/podcast-pocketcast.svg"/>
                                    </span>
                                    <span class="text">Pocketcast</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($podcast_links['apple']) : ?>
                            <li>
                                <a href="<?= $podcast_links['apple']; ?>" target="_blank" class="apple">
                                    <span class="icon">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/podcast-apple.svg"/>
                                    </span>
                                    <span class="text">Apple podcasts</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($podcast_links['google']) : ?>
                            <li>
                                <a href="<?= $podcast_links['google']; ?>" target="_blank" class="google">
                                    <span class="icon">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/podcast-google.svg"/>
                                    </span>
                                    <span class="text">Google podcasts</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($podcast_links['stitcher']) : ?>
                            <li>
                                <a href="<?= $podcast_links['stitcher']; ?>" target="_blank" class="stitcher">
                                    <span class="icon">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/podcast-stitcher.svg"/>
                                    </span>
                                    <span class="text">Stitcher</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ($podcast_links['spotify']) : ?>
                            <li>
                                <a href="<?= $podcast_links['spotify']; ?>" target="_blank" class="spotify">
                                    <span class="icon">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/podcast-spotify.svg"/>
                                    </span>
                                    <span class="text">Spotify podcasts</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($podcast_links['rss']) : ?>
                            <li>
                                <a href="<?= $podcast_links['rss']; ?>" target="_blank" class="rss">
                                    <span class="icon">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/podcast-rss.svg"/>
                                    </span>
                                    <span class="text">RSS Link</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
</main><!-- #main -->

<?php
get_footer();

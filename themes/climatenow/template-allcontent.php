<?php
/**
 * Template Name: All Content
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package climatenow
 */

get_header();
?>

<div class="filter-drawer">
    <a href="#" class="filter-drawer__close-btn"></a>
    <?php $allItemClasses = 'video podcast article '; ?>
    <div class="filter-drawer-section">
        <h3>Type</h3>
        <div class="filter-drawer-section button-group" data-filter-group="type">
            <ul>
                <li><button class="button" data-filter=".video">Video</button></li>
                <li><button class="button" data-filter=".podcast">Podcast</button></li>
                <?php if (get_field('hide-articles', 'options') != true) : ?>
                    <li><button class="button" data-filter=".article">Article</button></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <?php $categories = get_terms('category', array(
        'hide_empty' => true,
    )); 

    if ($categories) : ?>
        <div class="filter-drawer-section">
            <h3>Category</h3>
            <div class="button-group" data-filter-group="category">
                <ul>
                    <?php foreach( $categories as $term ): ?>
                        <li><button class="button" data-filter=".<?= str_replace(" ", "-", strtolower($term->slug)); ?>"><?= $term->name; ?></button></li>
                        <?php $allItemClasses .= str_replace(" ", "-", strtolower($term->slug)) . ' '; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>

    <?php $people = get_terms('person'); 
        if ($people) : ?>
        <div class="filter-drawer-section">
            <h3>Person</h3>
            <div class="button-group" data-filter-group="person">
                <ul>
                    <?php foreach( $people as $term ): ?>
                        <li><button class="button" data-filter=".<?= str_replace(" ", "-", strtolower($term->slug)); ?>"><?= $term->name; ?></button></li>
                        <?php $allItemClasses .= str_replace(" ", "-", strtolower($term->slug)) . ' '; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>
    <div class="filter-drawer__search-btn-wrapper">
        <a href="#" class="filter-drawer__search-btn">Show Media</a>
    </div>
</div>



<main id="primary" class="site-main">
    <div class="iso-grid-container">
        <?php 
        if (get_field('hide-articles', 'options') != true) {
            $args = array(
                'post_type' => array('article', 'video', 'podcast'),
                'posts_per_page' => -1,
                'meta_query' => array(
                    // Hides coming soon items from query
                    'relation' => 'OR',
                    array(
                        'key' => 'coming_soon',
                        'value' => '1',
                        'compare' => 'NOT EXISTS'
                    ),
                    array(
                        'key' => 'coming_soon',
                        'value' => '0',
                        'compare' => '=='
                    )
                )
            );
        } else {
            $args = array(
                'post_type' => array('video', 'podcast'),
                'posts_per_page' => -1,
                'meta_query' => array(
                    // Hides coming soon items from query
                    'relation' => 'OR',
                    array(
                        'key' => 'coming_soon',
                        'value' => '1',
                        'compare' => 'NOT EXISTS'
                    ),
                    array(
                        'key' => 'coming_soon',
                        'value' => '0',
                        'compare' => '=='
                    )
                )
            );
        }

        $query = new WP_Query( $args ); ?>
        <?php if ( $query->have_posts() ) : ?>
            <section>
                <div class="iso-grid-header">

                    <input type="text" class="iso-grid-search quicksearch" placeholder="What are you looking for?" />

                    <a href="#" class="filter-drawer-btn">Filter Media</a>
                    
                </div>
                <div class="iso-grid-wrapper"></div>
                <div class="iso-grid">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <?php $people = get_field('people');
                        if ($people) {
                            $personClass = '';
                            foreach( $people as $term ) {
                                $personClass .= str_replace(" ", "-", strtolower($term->slug)) . ' ';
                            }
                        } else {
                            $personClass = '';
                        }

                        $categories = get_the_terms( $post, 'category' ); 

                        if ($categories) {
                            $catClass = '';
                            foreach( $categories as $term ) {
                                $catClass .= str_replace(" ", "-", strtolower($term->slug)) . ' ';
                            }
                        } else {
                           $catClass = ''; 
                        } 

                        $host = get_field('host');
                        if ($host) {
                            $hostClass = '';
                            $hostNames = '';
                            foreach( $host as $term ) {
                                $hostClass .= str_replace(" ", "-", strtolower($term->slug)) . ' ';
                                $hostNames .= $term->name . ' ';
                            }
                        } else {
                            $hostClass = '';
                            $hostNames = '';
                        } ?>

                        <div class="iso-grid-item <?= $post->post_type; ?> <?= $personClass; ?> <?= $catClass; ?> <?= $hostClass; ?>">
                            <?php $seriesInfo = get_field('series'); 
                                  $episodeNumber = get_field('episode');
                                  $info = get_field('info'); ?>
                            
                            <a href="<?php the_permalink(); ?>" class="iso-grid-item__img">
                                <?= get_the_post_thumbnail(); ?>
                                <img class="iso-grid-item__img--icon" src="<?= get_template_directory_uri(); ?>/assets/icons/icon-<?= $post->post_type; ?>-white.svg"/>
                            </a>

                            <div class="single-tag label"><?= ucfirst($post->post_type); ?> <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?></div>

                            <div class="iso-grid-item__content">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <?php if (!$info['hide-summary']) : ?>
                                    <?php if ($info['summary']) : ?>
                                        <p><?= $info['summary']; ?></p>
                                    <?php else : ?>
                                        <?php if ($info['description']) : ?>
                                            <p><?= substr(strip_tags($info['description']), 0, 180); ?>...</p>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if ($host) : ?>
                                    <span style="font-size: 1px; color: transparent; height: 0; overflow: hidden;"><?= $hostNames; ?></span>
                                <?php endif; ?>
                            </div>
                            
                        </div>
                        
                    <?php endwhile; ?>
                </div>
            </section>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</main><!-- #main -->

<?php
get_footer();

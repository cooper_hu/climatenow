<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package climatenow
 */

$is_prod = stripos($_SERVER['HTTP_HOST'],"climatenow.com") !== false;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<?php if($is_prod) : ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-C6YM9RL2BE"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-C6YM9RL2BE');
		</script>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NH37WKM');</script>
		<!-- End Google Tag Manager -->
	<?php else : ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-Y99FX2HRPP"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'G-Y99FX2HRPP');
		</script>
	<?php endif; ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://kit.fontawesome.com/a7a81aeb4e.js" crossorigin="anonymous"></script>
	
	<?php wp_head(); ?>

	<style>
		/* WPADMIN BAR OVERRIDES */
		html { margin-top: 0 !important; }
		* html body { margin-top: 0 !important; }
		@media screen and ( max-width: 782px ) {
			html { margin-top: 0 !important; }
			* html body { margin-top: 0 !important; }
		}

		.card {
			visibility: hidden;
		}
	</style>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<?php /*
<div class="nav-bar" href="#">
	<a href="/" class="nav-bar__logo">
		<img src="<?= get_template_directory_uri(); ?>/assets/images/logo-climate-now.svg" alt="Climate Now Logo"/>
	</a>

	<a class="nav-bar__search" href="#">&nbsp;</a>
</div>

<a class="nav-bar__btn" href="#">
	<div class="hamburger-menu__wrapper">
		<div class="hamburger-menu"></div>
	</div>
</a>

<nav class="primary-nav">
	<?php
	wp_nav_menu(
		array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
		)
	);

	wp_nav_menu(
		array(
			'theme_location' => 'header-right',
			'menu_class'     => 'nav-right',
			'menu_id'        => 'primary-menu',
		)
	); ?>
</nav>
*/ ?>

<div class="superheader">
	<a href="/" class="superheader__logo">
		<img src="<?= get_template_directory_uri(); ?>/assets/images/logo-climate-now.svg" alt="Climate Now Logo"/>
	</a>

	<a class="nav-bar__search" href="#">&nbsp;</a>
</div>

<a class="nav-bar__btn" href="#">
	<div class="hamburger-menu__wrapper">
		<div class="hamburger-menu"></div>
	</div>
</a>

<nav class="supernav">
	
	<?php
	wp_nav_menu(
		array(
			'theme_location' => 'menu-1',
			'menu_class'     => 'js-dd-nav',
			'menu_id'        => 'primary-menu',
			'link_before'    => '<span>',
  			'link_after'     => '</span>',
		)
	); ?>

	<div>
		<div class="supernav__search--wrapper">
			<?php wp_nav_menu(
				array(
					'theme_location' => 'header-right',
					'menu_class'     => 'nav-right',
					'menu_id'        => 'primary-menu',
					'depth' 		 => 1,
					// 'items_wrap'     => '<div id="%1$s" class="%2$s">%3$s <span>Hi</span></div>',
					// 'link_before'    => '<span>',
		  			// 'link_after'     => '</span>'
				)
			); ?>

			<!--
				<a class="supernav__search" href="#">&nbsp;</a>
			-->

		</div>
	</div>
</nav>


<?php if (get_field('show-webby', 'options')) : ?>
	<a class="webby-badge site-wide" href="https://winners.webbyawards.com/2022/video/video-series-channels/science-education/206146/climate-now" target="_blank">
		<img src="<?= get_template_directory_uri(); ?>/assets/images/webby-badge-2.png"/>
	</a>
<?php endif; ?>

<?php get_template_part( 'template-parts/content-taxnav' ); ?>
<?php get_template_part( 'template-parts/search-modal' ); ?>

<?php if (is_singular('video') || is_singular('podcast') || is_singular('article')) : ?>
	<a class="content-nav-btn <?php the_field('header_text_color'); ?>" href="#">&nbsp;</a>
<?php endif; ?>

<div class="site-wrapper">
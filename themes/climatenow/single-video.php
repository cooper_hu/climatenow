<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package climatenow
 */

get_header(); ?>

<main id="primary" class="single-page-wrapper">
    <article>
        <?php get_template_part('template-parts/single-header'); ?>
        <div class="single-content">
            <div class="container--single">

                <div class="col-smD-4">
                    <?php get_template_part('template-parts/section-people'); ?>
                    <?php get_template_part('template-parts/section-host'); ?>
                    <?php get_template_part('template-parts/section-related-media'); ?>
                </div>
            </div>
            <div class="container--transcript">
                <div id="transcript" class="single-video-transcript-anchor"></div>
                <?php get_template_part('template-parts/single-transcript'); ?>
            </div>
        </div>
    </article>
</main><!-- #main -->

<?php get_template_part('template-parts/share-modal'); ?>
<?php
get_footer();

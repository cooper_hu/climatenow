<?php
/**
 * Template Name: Events
 The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package climatenow
 */

get_header();
?>

<main id="primary" class="site-main standard-page">
    <div class="container--single">
        <div class="col-smD-8 col-lgD-6 no-padding">
            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <header class="entry-header event-header">
                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                    </header><!-- .entry-header -->

                    <div class="entry-content ">
                        <?php // Content/Description
                        the_field('description'); ?>

                        <?php $download_btn = get_field('pdf_download');
                        if ($download_btn) : ?>
                            <div class="event-pdf-section">
                                <a href="<?= $download_btn['url']; ?>" target="_blank" class="btn"><?= $download_btn['label']; ?></a>
                            </div>
                        <?php endif; ?>
                        
                        <?php // Gallery
                        $images = get_field('gallery');
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        if( $images ): ?>
                            <div class="event-media-header">
                                <?php if (get_field('gallery_headline')) : ?>
                                    <h4><?= get_field('gallery_headline'); ?></h4>
                                <?php endif; ?>
                            </div>
                            <div class="event-gallery">
                                <?php foreach( $images as $image ): ?>
                                    <div class="event-gallery__slide">
                                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" width="<?= $image['width']; ?>" height="<?= $image['height']; ?>"/>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                           
                            <div class="event-gallery-caption">
                                <?php foreach( $images as $image ): ?>
                                    
                                    <div class="event-gallery-caption__slide">
                                        <?php if ($image['caption']) : ?>
                                            <p><?= $image['caption']; ?></p>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
            
                        <?php endif; ?>
                        
                        <?php // Videos ?>
                        <?php if ( have_rows('videos') ): ?>
                            <div class="video-gallery">
                                <div class="event-media-header">
                                    <?php if (get_field('videos_headline')) : ?>
                                        <h4><?= get_field('videos_headline'); ?></h4>
                                    <?php endif; ?>
                                </div>
                                <?php while( have_rows('videos') ): the_row(); ?>
                                    <div class="event-video-wrapper">
                                        <div class="embed-container">
                                            <?php the_sub_field('video'); ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        
                            <?php if (get_field('video_description')) : ?>
                                <div class="video-description">
                                    <?= get_field('video_description'); ?>
                                </div>
                            <?php endif; ?>
                        
                        <?php endif; ?>

                        <?php // Partner Logos 
                        $images = get_field('logos');
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        if( $images ): ?>
                            <div class="event-media-header">
                                <?php if (get_field('partners_headline')) : ?>
                                    <h4><?= get_field('partners_headline'); ?></h4>
                                <?php endif; ?>
                            </div>
                            <div class="partner-logos">
                                <?php foreach( $images as $image ): ?>
                                    <div class="partner-logos-item">
                                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" width="<?= $image['width']; ?>" height="<?= $image['height']; ?>"/>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        
                    </div><!-- .entry-content -->

                </article><!-- #post-<?php the_ID(); ?> -->

            <?php endwhile; ?>
        </div>
    </div>

</main><!-- #main -->

<?php
get_footer();

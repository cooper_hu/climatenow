jQuery(function($){
  // Calculate min-heights for cover page.
  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  /* ******************************************************
   * Sitewide: Navigation
   * ****************************************************** */
  // Primary Mobile Nav
  $('.nav-bar__btn').on('click', function(e) {
      e.preventDefault();
      // $('.primary-nav').toggleClass('visible');
      $('.supernav').toggleClass('visible');
      $('.hamburger-menu').toggleClass('animate');
      $('.site-wrapper').toggleClass('shift-down');
      $('body').toggleClass('fixed-primary-nav');

      $('.search-drawer').removeClass('visible');
      $('body').removeClass('fixed-search');
      $('.nav-bar__search').removeClass('active');
  });

  $('.js-dd-nav li.menu-item-has-children > a').on('click', function(e) {
    if ($(window).width() <= 1199) {
      e.preventDefault();
      console.log('Mobile');

      var subnavitem = $(this).parent().find('.sub-menu');

      subnavitem.toggleClass('visible');
      
      if (subnavitem.hasClass('visible')) {
        gsap.to(subnavitem, { height: 'auto', duration: 0.35 });
      } else {
        gsap.to(subnavitem, { height: 0, duration: 0.35 });
      }

      $(this).parent().find('> a').toggleClass('visible');
    } else {
      // e.preventDefault();
      console.log('Desktop');
    }
  });

  $('.js-dd-nav li.menu-item-has-children > a, .js-dd-nav li.menu-item-has-children .sub-menu').on('mouseover', function(e) {
    if ($(window).width() > 1199) {
      var subnavitem = $(this).parent().find('.sub-menu');
      subnavitem.addClass('hovered');
      $(this).parent().find('> a').addClass('hovered');
    }
  });

  $('.js-dd-nav li.menu-item-has-children > a, .js-dd-nav li.menu-item-has-children .sub-menu').on('mouseleave', function(e) {
    if ($(window).width() > 1199) {
      var subnavitem = $(this).parent().find('.sub-menu');
      subnavitem.removeClass('hovered');
      $(this).parent().find('> a').removeClass('hovered');
    }
  });

  $('.sub-menu a').on('click', function() {
    // Remove nav stuff if internal liink is clicked
    $('.supernav').removeClass('visible');
    $('.hamburger-menu').removeClass('animate');
    $('.site-wrapper').removeClass('shift-down');
    $('body').removeClass('fixed-primary-nav');
  });


  // All Content filter drawer
  $('.filter-drawer-btn').on('click', function(e) {
    e.preventDefault();
    $('.filter-drawer').toggleClass('visible');
    $('body').toggleClass('fixed');
  });

  $('.filter-drawer__close-btn, .filter-drawer__search-btn').on('click', function(e) {
    e.preventDefault();
    $('.filter-drawer').removeClass('visible');
    $('body').removeClass('fixed');
  });

  $('.nav-bar__search, .super-nav-search a').on('click', function(e) {
    e.preventDefault();
    $('.search-drawer').toggleClass('visible');
    $('.nav-bar__search').toggleClass('active');
    $('.super-nav-search a').toggleClass('active');
    $('body').toggleClass('fixed-search');

    if ($('.primary-nav').hasClass('visible')) {
      $('.primary-nav').removeClass('visible');
      $('.hamburger-menu').removeClass('animate');
      $('.site-wrapper').removeClass('shift-down');
      // $('body').removeClass('fixed-search');
      $('body').removeClass('fixed-primary-nav');
    }
  });

  // current-menu-item
  $('.current-menu-item a').on('click', function(e) {
    
    var url = this.getAttribute("href");
    var url_parts = url.replace(/\/\s*$/,'').split('/'); 

    console.log('url: ', url_parts);

    var archive_type = $('#primary').data('archive');

    if (archive_type === url_parts[1]) {
      console.log('on the correct page!');
      e.preventDefault();

      if ($(window).width() >= 1199) {
        var offsetsize = 0 ;
      } else {
        var offsetsize = -360;
      }
      
      $(window).scrollTo($(url_parts[2]), {
        duration: 500,
        offset: offsetsize,
      });

    }
  })

  /* **************************
   * Newsletter Form
   * ************************** */ 
  $('.newsletter-form').submit(function (e) {
      e.preventDefault();
      var formMessage = $('.newsletter-form p');

      // Reset message
      formMessage.removeClass('visible success');

      if ($(this).find('.newsletter-form__email').val() === '') {
        $(this).find('p.error').addClass('visible');
      } else {
        var action = $(this).attr('action');
        var $this = $(this);
        $.ajax({
          url: action,
          type: 'POST',
          data: {
            newsletter_email: $this.find('.newsletter-form__email').val()
          },
          success: function (data) {
            data = jQuery.parseJSON(data);

            console.log(data);

            if (data.title === "Member Exists") {
              // console.log(data);
              formMessage.html($this.find('.newsletter-form__email').val() + ' is already a subscriber.').addClass('visible');
            } else if (data.status === 'subscribed') {
              $this.addClass('success');
              formMessage.html('Thank you for subscribing.').addClass('success');
              $('body').append('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3256540&conversionId=4411116&fmt=gif"/>');
            }
          }
        });
      }
  });

  /* ******************************************************
   * Search
   * ****************************************************** */
  // ALL SEARCH JS CODE IS ON /assets/js/js-filtering.js

  /* ******************************************************
   * Homepage
   * ****************************************************** */
  $('.hp-intro__scroll-link').on('click', function(e) {
    $(window).scrollTo($('#hpContent'), {
        duration: 500,
        offset: -150,
    });
  });

  if ($('body').hasClass('page-template-template-homepage')) {
    var controller = new ScrollMagic.Controller();

    var hp_ani_intro = new TimelineMax()
      .from(".nav-bar__logo", 0.5,
        {x: -300}, "end"
      )
      .to(".js-hp-fadeout", 0.5,
        {opacity: 0}, "end"
      );

    var scene1 = new ScrollMagic.Scene({
      triggerElement: ".js-ani-navlogo",
      triggerHook: 0.05,
      duration: 300,
      // offset: 100
    })
    .setTween(hp_ani_intro) // trigger a TweenMax.to tween
    // .addIndicators({name: "1 (duration: 0)"}) // add indicators (requires plugin)
    .addTo(controller);

    
    $('.hp-intro-trailer').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade mfp-video-single',
      removalDelay: 160,
      preloader: false,
      closeBtnInside: false,
      fixedContentPos: true,
    }); 
  }

  /* ******************************************************
   * Archive Pages
   * ****************************************************** */
  if ($('body').hasClass('archive')) {
    var slickSettings = {
      slidesToShow: 6,
      slidesToScroll: 1,
      infinite: false,
      accessibility: true,
      variableWidth: true,
      responsive: [{
        breakpoint: 2100,
        settings: {
          slidesToShow: 5
        }
      }]
    };

    // $('.archive-carousel').slick(slickSettings);
    $('.archive-carousel').slick(slickSettings);
    $('.archive-carousel').css('opacity', 1)

    /* */
    var slideUp = {
      distance: '30px',
      origin: 'bottom',
      opacity: 0,
      interval: 150,
      // cleanup: true,
      afterReveal: function (el) {
        el.classList.remove('animated');
      }
    };
    
    // ScrollReveal().reveal('.slick-slide .card.animated', slideUp);
    // ScrollReveal().reveal('.scrolling-wrapper .card.animated', slideUp);

    ScrollReveal().reveal('.card', slideUp);
  }


  /* ******************************************************
   * All Content Page
   * ****************************************************** */
  if ($('body').hasClass('page-template-template-allcontent')) {

    // store filter for each group
    var buttonFilters = {};
    var buttonFilter;
    // quick search regex
    var qsRegex;

    // init Isotope
    var $grid = $('.iso-grid').isotope({
      itemSelector: '.iso-grid-item',
      layoutMode: 'fitRows',
      filter: function() {
        var $this = $(this);
        var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
        var buttonResult = buttonFilter ? $this.is( buttonFilter ) : true;
        return searchResult && buttonResult;
      },
    });

    // bind event listener to be triggered just once. note ONE not ON
    $grid.one( 'arrangeComplete', function() {
      console.log('arrange done, just this one time');
      $('.iso-grid-item').css('opacity', 1);
    });

    $('.iso-grid').css('opacity', 1);

    $('.filter-drawer').on( 'click', '.button', function() {
      var $this = $(this);
      var filterValue;

      // get group key
      var $buttonGroup = $this.parents('.button-group');
      var filterGroup = $buttonGroup.attr('data-filter-group');

      if (!$this.is('.is-checked')) {
        // Uncheck the box
        buttonFilters[ filterGroup ] = '*';
        buttonFilter = concatValues( buttonFilters );

      } else {
        // set filter for group
        buttonFilters[ filterGroup ] = $this.attr('data-filter');
        // combine filters
        buttonFilter = concatValues( buttonFilters );
      }

      // console.log('buttonFilters', buttonFilters);
      // console.log('filterGroup', filterGroup);

      // Isotope arrange
      // $('.iso-grid-item').each(function() { $(this).css('opacity', 1); });
      $grid.isotope();
    });

    // use value of search field to filter
    var $quicksearch = $('.quicksearch').keyup( debounce( function() {
      qsRegex = new RegExp( $quicksearch.val(), 'gi' );
      $grid.isotope();
    }) );

    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        if ($(this).hasClass('is-checked')) {
          $( this ).removeClass('is-checked');
        } else {
          $buttonGroup.find('.is-checked').removeClass('is-checked');
          $( this ).addClass('is-checked');
        }
      });
    });
    
    // flatten object by concatting values
    function concatValues( obj ) {
      var value = '';
      for ( var prop in obj ) {
        value += obj[ prop ];
      }
      return value;
    }

    // debounce so filtering doesn't happen every millisecond
    function debounce( fn, threshold ) {
      var timeout;
      threshold = threshold || 100;
      return function debounced() {
        clearTimeout( timeout );
        var args = arguments;
        var _this = this;
        function delayed() {
          fn.apply( _this, args );
        }
        timeout = setTimeout( delayed, threshold );
      };
    }

    var slideUp = {
      distance: '30px',
      origin: 'bottom',
      opacity: 0,
      interval: 200,
    };
  

    ScrollReveal().reveal('.iso-grid-item', slideUp);
  }

  /* ******************************************************
   * Single Page (All)
   * ****************************************************** */

  $('.content-nav-btn').on('click', function(e) {
      e.preventDefault();
      $('.subnav-drawer').toggleClass('visible');
      $('.nav-bar__btn').toggleClass('visible');
      $('.site-wrapper').toggleClass('shifted');
      $('body').toggleClass('fixed');

      console.log('test?');

      // Fire reveal animation
      gsap.from(".subnav-series.active .ani-stagger-in", {
        x: -20,
        opacity: 0,
        stagger: 0.1, 
        delay: 0.05
         // 0.1 seconds between when each ".box" element starts animating
        // delay: 500,
      });
  });

  $('.subnav-drawer__close-btn').on('click', function(e) {
      e.preventDefault();
      $('.subnav-drawer').removeClass('visible');
      $('.nav-bar__btn').removeClass('visible');
      $('.site-wrapper').removeClass('shifted');
      $('body').removeClass('fixed');
  });


  $('.subnav-drawer__taxonomy-nav a').on('click', function(e) {
    e.preventDefault();
    var type = $(this).data('type');
    console.log(type);

    // Update active class
    if (!$(this).hasClass('active')) {
      // Remove item active class
      $('.subnav-drawer__taxonomy-nav a').each(function(e) {
        $(this).removeClass('active');
      });

      // Remove parent active class
      $('.subnav-drawer__taxonomy-nav').removeClass('podcasts videos articles');

      // remove active class off group bucket
      $('.subnav-series').each(function(e) {
        $(this).removeClass('active');
      });

      // a > li > ul > nav.subnav-drawer__taxonomy-nav
      $('.subnav-drawer__taxonomy-nav').addClass(type.toLowerCase());

      gsap.from("#subnavSection"+type+" .ani-stagger-in", {
        x: -20,
        opacity: 0,
        stagger: 0.1, 

         // 0.1 seconds between when each ".box" element starts animating
        // delay: 500,
      });

      // set nav item and group active
      $('#subnavSection'+type).addClass('active');
      $(this).addClass('active');
    }
  });

  if ($('body').hasClass('single')) {
    console.log('single page');

    // Set active class on page load
    var pathname = window.location.pathname.split('/').filter(function(v){return v!==''});
    console.log(pathname[0]);

    if (pathname[0] === 'video') {
      $('.subnav-drawer__taxonomy-nav').addClass('videos');
      $('#subnavCatVideos').addClass('active');
      $('#subnavSectionVideos').addClass('active');

      getSubnavActiveClass('#subnavSectionVideos');
    } else if (pathname[0] === 'podcast') {
      $('.subnav-drawer__taxonomy-nav').addClass('podcasts');
      $('#subnavCatPodcasts').addClass('active');
      $('#subnavSectionPodcasts').addClass('active');

      getSubnavActiveClass('#subnavSectionPodcasts');
    } else {
      $('.subnav-drawer__taxonomy-nav').addClass('articles');
      $('#subnavCatArticles').addClass('active');
      $('#subnavSectionArticles').addClass('active');

      getSubnavActiveClass('#subnavSectionArticles');
    }

    // Initiate person popup
    initPersonPopup();
  }

  /* ******************************************************
   * Homepage
   * ****************************************************** */
  if ($('body').hasClass('page-template-template-homepage')) {
    // Initiate person popup
    initPersonPopup();

    $('.hp-features-item__scroll-link').on('click', function(e) {
      e.preventDefault();
      var scrollDest = $(this).data('dest');

      var scrollOffset = -70;
      if($(window).width() > 1199) scrollOffset = -100;

      $(window).scrollTo($(scrollDest), {
        duration: 500,
        offset: scrollOffset,
      });
    });
  }


  $('.single-share').on('click', function(e) {
    e.preventDefault();
  })

  /* ******************************************************
   * Single Page (Podcast)
   * ****************************************************** */
  if ($('body').hasClass('single-podcast')) {
    const podcastTitle = $('.single-media__cover').data('title');
    const podcastURL = $('.single-media__cover').data('mp3');

    // Podcast
    Amplitude.init({
      "preload": 'none',
      "bindings": {
        37: 'prev',
        39: 'next',
        // 32: 'play_pause'
      },
      "songs": [
        {
          "name": podcastTitle,
          "artist": "Climate Now",
          "url": podcastURL,
        }
      ]
    }); 

    $('#single-song-player').css('opacity', 1);

    /*
    window.onkeydown = function(e) {
        return !(e.keyCode == 32);
    };*/

    document.getElementById('song-played-progress').addEventListener('click', function( e ){
      var offset = this.getBoundingClientRect();
      var x = e.pageX - offset.left;

      Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
    });

    $('.single-media__cover.single-page').on('click', function(e) {
      e.preventDefault();
      $(this).fadeOut();
      $('.amplitude-play-pause').click();

       
      var label = $('.single-title').html();
      // Event Tracking

      gtag('event', 'Download', {
        'event_category': 'Pocast',
        'event_label': label,
      });
    });
  }

  /* ******************************************************
   * Single Page (Video)
   * ****************************************************** */
  if ($('body').hasClass('single-video')) {
    $('.single-play-btn').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade mfp-video-single',
      removalDelay: 160,
      preloader: false,
      closeBtnInside: false,
      fixedContentPos: true,
    });

    $('.single-play-btn').on('mfpOpen', function(e /*, params */) {
      // console.log('Popup opened',  $.magnificPopup.instance);

      var label = $('.single-title').html();

      gtag('event', 'View', {
        'event_category': 'Video',
        'event_label': label,
      });

      // console.log ('GA', 'Play, Video, ' + label);
    });
  }

  /* ******************************************************
   * Single Page (Article)
   * ****************************************************** */
  if ($('body').hasClass('single-article')) {
    $('.single-read-btn').on('click', function(e) {
      e.preventDefault();

      var scrollOffset = -50;
      if($(window).width() > 1199) scrollOffset = -78;

      $(window).scrollTo($('.single-content'), {
        duration: 500,
        offset: scrollOffset,
      });
    });
  }

  // Sets the active class of the subnav item on page load 
  function getSubnavActiveClass(id) {
    $(id + ' a').each(function(e) {
      if ($(this)[0].pathname === window.location.pathname) {
        $(this).addClass('active');
        $(this).click(false);
      }
    });
  }

  function initPersonPopup() {
    // Initiate person popup
    $('.single-people__item').magnificPopup({
      type:'inline',
      mainClass: 'mfp-fade mfp-person',
      midClick: true,
      closeBtnInside: true,
      fixedContentPos: true,
      closeOnBgClick: true,
    });
  }

  // All Single
  $('.single-share').magnificPopup({
    type:'inline',
    mainClass: 'mfp-fade mfp-share',
    midClick: true,
    closeBtnInside: true,
    fixedContentPos: true,
    closeOnBgClick: true,
  });


  /* Partner */
  /* ******************************************************
   * Partners Page Template
   * ****************************************************** */
  if ($('body').hasClass('page-template-template-partners')) {
    $('.js-show-form').magnificPopup({
      type:'inline',
      mainClass: 'mfp-fade mfp-partners',
      midClick: true,
      closeBtnInside: true,
      fixedContentPos: true,
      closeOnBgClick: true,
    });
  }


  /* ******************************************************
   * Single Page (Article/Video)
   * ****************************************************** */
  if ($('body').hasClass('single-article') || $('body').hasClass('single-video')) {
    var controller = new ScrollMagic.Controller();

    var single_ani_header = new TimelineMax()
      .to(".js-ani-fadeout", 0.5,
        {opacity: 0}, "end"
      );

    var scene1 = new ScrollMagic.Scene({
      triggerElement: ".js-ani-fadeout-trigger",
      triggerHook: 0.05,
      duration: 400,
      // offset: 100
    })
    .setTween(single_ani_header) // trigger a TweenMax.to tween
    // .addIndicators({name: "1 (duration: 0)"}) // add indicators (requires plugin)
    .addTo(controller);

    new ScrollMagic.Scene({
      triggerElement: ".single-content"
    })
    .setClassToggle(".content-nav-btn", "black") // add class toggle
    // .addIndicators() // add indicators (requires plugin)
    .addTo(controller);
  }

  /* ******************************************************
   * Event Page Template
   * ****************************************************** */
  if ($('body').hasClass('page-template-template-events')) {
    $(window).on('load resize', function () {
      changepos();
    });

    function changepos() {
      var toppos = ($('.slick-active').find("img").height()/2);
      $('.slick-arrow').css('top',toppos+'px');
    }

    $('.event-gallery').slick({
      fade: true,
      lazyLoad: 'ondemand',
      adaptiveHeight: true,
      arrows: false,
      
      asNavFor: '.event-gallery-caption'
    });

    $('.event-gallery-caption').slick({
      fade: true,
      // 
      asNavFor: '.event-gallery'
    })
  }

  /* "Read More" button functionality
  if ($('body').hasClass('single-article') || $('body').hasClass('single-video') || $('body').hasClass('single-podcast')) {
    // Check to see if you want to add a "Read more"
    
    $('#pageContent').imagesLoaded(function() {
      // images have loaded
      var contentHeight = $('#pageContent').innerHeight();
      
      
      if (contentHeight < 1000) {
        // console.log('Read More not needed');
        $('#readMoreBtn').remove();
      } else {
        // console.log('Needs a Read More');
        $('.single-transcript').addClass('expandable');
        $('.single-transcript__inner').addClass('collapsed');

        $('#readMoreBtn').on('click', function(e) {
          e.preventDefault();
          if ($(this).hasClass('state-less')) {
            $('.single-transcript__inner').css({
              height: '40.5rem',
            });

            $('.single-transcript__inner').addClass('collapsed');
            $(this).removeClass('state-less');
            $(this).html('Read More');

          } else {
            var innerHeight = $('.content-wrapper').innerHeight();

            $('.single-transcript__inner').css({
              height: innerHeight,
            });

            $('.single-transcript__inner').removeClass('collapsed');
            $(this).addClass('state-less');
            $(this).html('Read Less');
          }
        })
      }
    });
    
    $(window).resize(function () {
      waitForFinalEvent(function(){
        if ($('.single-transcript').hasClass('expandable') && $('#readMoreBtn').hasClass('state-less')) {
          var innerHeight = $('.content-wrapper').innerHeight();

          $('.single-transcript__inner').css({
            height: innerHeight,
          });
        }
      }, 250);
    });
    
  }
  */
});

var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

/* ***************************
 * Popup Share
 * *************************** */
;(function ($) {
    $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {

        // Prevent default anchor event
        e.preventDefault();

        var strResize;

        // Set values for window
        intWidth = intWidth || '500';
        intHeight = intHeight || '400';
        strResize = (blnResize ? 'yes' : 'no');

        // Set title and open popup with focus on it
        var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
            strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
            objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
    }

    /* ================================================== */
    $(document).ready(function ($) {
        $('.share-popup__icon.js-popup').on("click", function (e) {
            // console.log('Clicked');
            $(this).customerPopup(e);
        });
    });
}(jQuery));

/* ******************************************************
   * Constant Contact
   * ****************************************************** */
function applyWhenElementExists(selector, myFunction, intervalTime) {
   var interval = setInterval(function() {
     if (jQuery(selector).length > 0) {
        myFunction();
        clearInterval(interval);
     }
   }, intervalTime);
 }

applyWhenElementExists("#email_address_0", function(){
  // Add Headline to homepage
  var headline = $('#hpNewsletterHeadlineWrapper').html();
  $('#hpNewsletter').find('form').prepend(headline);
  // Add modal button
  $('#email_address_field_0').append('<a href="#" class="newsletter-popup-btn"><i class="fas fa-question-circle"></i></a>')
  // Setup consent toggle button
  $('.newsletter-popup-btn').on('click', function(e) {
    e.preventDefault();

    $('#gdpr_text').toggleClass('fixed');
  });

  $('.newsletter-popup-btn').on('mouseover', function(e) {
    $('#gdpr_text').addClass('visible');
  });

  $('.newsletter-popup-btn').on('mouseleave', function(e) {
    $('#gdpr_text').removeClass('visible');
  });

  document.getElementById("email_address_0").placeholder = "Email Address*";
}, 50);

applyWhenElementExists("#email_address_1", function(){
  var headline = $('#footerNewsletterHeadlineWrapper').html();
  $('#footerNewsletter').find('form').prepend(headline);
  
  document.getElementById("email_address_1").placeholder = "Email Address*";
}, 50);


<?php $seriesInfo       = get_field('series');
      $episodeNumber    = get_field('episode'); 
      $info             = get_field('info'); ?>
<div class="search-drawer-item">
    <div class="single-tag label">
        <?= ucfirst($post->post_type); ?> <?php if ($post->post_type != 'article') { echo 'Episode'; } ?> <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?>
    </div>
    
    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

    <?php if (!$info['hide-summary']) : ?>
        <?php if ($info['summary']) : ?>
            <p><?= $info['summary']; ?></p>
        <?php else : ?>
            <?php if ($info['description']) : ?>
                <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
</div>
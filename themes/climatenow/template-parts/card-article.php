<?php 
$iconUrl = get_template_directory_uri() . '/assets/icons/icon-article-black.svg';
$relatedLabel = 'Article';

$seriesInfo    = get_field('series');
$episodeNumber = get_field('episode');
$youtubeLink   = get_field('youtube');
$runtime       = get_field('runtime');
$contentGroup  = get_field('info');

$people = get_field('people');
$relatedMedia = get_field('related'); 

$bg_type      = get_field('header_type');
$header_image = get_field('header_image');
$header_video = get_field('header_video');

$section_id = 'article-' . get_field('series_number', $seriesInfo) . '-' . $episodeNumber; ?>


<section id="<?= $section_id; ?>">
    <a href="#" data-dest="#<?= $section_id; ?>" class="hp-features-item__scroll-link">Recent Article</a>

    <div class="col-smD-4">
        <div class="single-tag">Article <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?></div>

        <h1 class="single-title <?php if ($contentGroup['subheadline']) echo 'margin-bottom-sm'; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

        <?php if ($contentGroup['subheadline']) : ?>
            <h2 class="single-title--sm" style="margin-top: 0;"><?= $contentGroup['subheadline']; ?></h2>
        <?php endif; ?>
    </div>

    <div class="col-smD-flex">
        <div class="col-smD-3">
            <div class="single-description">
                <?= $contentGroup['description']; ?>
            </div>

            <div class="single-podcast-header-break small"></div>

            <?php get_template_part('template-parts/section-people'); ?>
        </div>
        <div class="col-smD-7">
            <div class="single-media">
                <span class="single-media__date">Date: <?= get_the_date('m.d.Y'); ?></span>
                <div class="single-media__inner">
                    <a href="<?php the_permalink(); ?>" class="single-media__cover">
                        <?php if ($bg_type === 'image') : ?>
                            <div class="single-media__cover--img" style="background-image: url(<?= $header_image['url']; ?>);"></div>
                        <?php elseif ($bg_type === 'video') : ?>
                            <video 
                                class="single-media__inner--video" 
                                src="<?= $header_video; ?>" 
                                preload="none"
                                role="video" 
                                control="false"
                                autoplay 
                                loop 
                                muted 
                                ></video>
                        <?php else : ?>
                            <div class="single-media__cover--img" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);"></div>
                        <?php endif; ?>
                        <div class="single-media__cover--text">
                            <span class="single-podcast-btn">
                                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-article-white.svg"/>
                                <span>Read Now</span>
                            </span>
                        </div>
                    </a>
                </div>
                <?php if ($runtime) : ?>
                    <span class="single-media__running-time">Reading Time: <?= $runtime; ?> <span>min</span></span>
                <?php endif; ?>
            </div> 

            <div class="single-related-wrapper flex">
                <div class="host">
                    <?php get_template_part('template-parts/section-author'); ?>
                </div>
                <div class="related">
                    <?php get_template_part('template-parts/section-related-media'); ?>
                </div>
            </div>
        </div>
    </div>
</section>



            
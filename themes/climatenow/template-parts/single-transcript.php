<?php if( have_rows('transcript_flex') ) : ?>
    
    <section class="single-transcript">
        <div class="single-transcript__inner">
            <div class="content-wrapper" id="pageContent">
                <?php if ($post->post_type === 'video') : 
                    $seriesInfo    = get_field('series');
                    $episodeNumber = get_field('episode'); ?>
                    <div class="single-transcript__header">
                        <div class="transcript-tag">Video Episode <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?> <span>Transcript</span></div>
                    </div>
                <?php endif; ?>
                
                <?php while ( have_rows('transcript_flex') ) : the_row(); ?>
                    <?php if( get_row_layout() == 'block-text' ):
                        $timestamp = get_sub_field('timestamp'); ?>

                        <div class="transcript-item">
                            <?php if ($timestamp) : ?>
                                <span class="transcript-item__timestamp"><?= $timestamp; ?></span>
                            <?php endif; ?>
                            <?php the_sub_field('text'); ?>
                        </div>

                    <?php elseif( get_row_layout() == 'block-image' ): 
                        $timestamp = get_sub_field('timestamp');
                        $image = get_sub_field('image'); ?>

                        <div class="transcript-item">
                            <?php if ($timestamp) : ?>
                                <span class="transcript-item__timestamp"><?= $timestamp; ?></span>
                            <?php endif; ?>
                            <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                        </div>

                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="single-transcript__footer">
            <div class="transcript-item">
                <div class="single-transcript__footer--flex">
                    <div class="left">
                        <?php /* <a href="#" class="btn" id="readMoreBtn">Read More</a> */ ?>
                    </div>
                    <div class="right">
                        <a href="<?php the_field('all-content-page','options'); ?>" class="btn accent--left animate">All Content</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php endif; ?>
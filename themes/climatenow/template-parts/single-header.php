<?php // BG Header specific fields.
$bg_type           = get_field('header_type');
$header_image      = get_field('header_image');
$header_video      = get_field('header_video');
$poster_image      = get_field('header_video_poster');
$header_text_color = get_field('header_text_color');
$header_overlay    = get_field('header_overlay'); 
if ($header_text_color === 'white') {
    $icon_color = '-white';
} else {
    $icon_color = '-black';
} ?>

<?php if ($bg_type === 'image') : ?>
    <header class="single-bg-header <?php if ($header_overlay) echo 'overlay '; echo $header_text_color; ?>" style="background-image: url(<?= $header_image['url']; ?>);">
        <?php get_template_part('template-parts/single-header-content'); ?>
        <?php if ($post->post_type === 'video') : ?>
            <a href="<?php the_field('youtube', false, false); ?>" class="single-play-btn js-ani-fadeout">
                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-video<?=$icon_color; ?>.svg"/>
                <span>Watch Video</span>
            </a>
        <?php elseif ($post->post_type === 'article') : ?>
            <a href="#" class="single-read-btn js-ani-fadeout">
                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-article<?=$icon_color; ?>.svg"/>
            </a>
        <?php endif; ?>
    </header>
<?php elseif ($bg_type === 'video') : ?>
    <header class="single-bg-header <?php if ($header_overlay) echo 'overlay '; echo $header_text_color; ?>">
        <video 
            class="single-bg-header__video" 
            src="<?= $header_video; ?>" 
            preload="none"
            role="video" 
            control="false"
            autoplay 
            playsinline
            loop 
            muted 
            ></video>
        <?php get_template_part('template-parts/single-header-content'); ?>
        <?php if ($post->post_type === 'video') : ?>
            <a href="<?php the_field('youtube', false, false); ?>" class="single-play-btn js-ani-fadeout">
                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-video<?=$icon_color; ?>.svg"/>
                <span>Watch Video</span>
            </a>
        <?php elseif ($post->post_type === 'article') : ?>
            <a href="#" class="single-read-btn js-ani-fadeout">
                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-article<?=$icon_color; ?>.svg"/>
            </a>
        <?php endif; ?>
    </header>
<?php else : ?>
    <header class="single-bg-header <?php if ($header_overlay) echo 'overlay '; echo $header_text_color; ?>" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);">
        <?php get_template_part('template-parts/single-header-content'); ?>
        <?php if ($post->post_type === 'video') : ?>
            <a href="<?php the_field('youtube', false, false); ?>" class="single-play-btn js-ani-fadeout">
                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-video<?=$icon_color; ?>.svg"/>
                <span>Watch Video</span>
            </a>
        <?php elseif ($post->post_type === 'article') : ?>
            <a href="#" class="single-read-btn js-ani-fadeout">
                <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-article<?=$icon_color; ?>.svg"/>
            </a>
        <?php endif; ?>
    </header>
<?php endif; ?>
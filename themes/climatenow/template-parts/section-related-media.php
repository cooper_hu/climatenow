<?php 
$relatedMedia = get_field('related');

if ($relatedMedia) : ?>
    <h3 class="single-headline">Related Media:</h3>
    <div class="single-related">
        <?php foreach( $relatedMedia as $post ): 
            setup_postdata($post); 
            $type = $post->post_type ?>
            <?php if ($type === 'video') {
                $iconUrl = get_template_directory_uri() . '/assets/icons/icon-video-black.svg';
                $relatedLabel = 'Video';
            } elseif ($type === 'podcast') {
                $iconUrl = get_template_directory_uri() . '/assets/icons/icon-podcast-black.svg';
                $relatedLabel = 'Podcast';
            } else {
                $iconUrl = get_template_directory_uri() . '/assets/icons/icon-article-black.svg';
                $relatedLabel = 'Article';
            } ?>
            <a href="<?php the_permalink(); ?>" class="single-related__item">
                <div class="single-related__item--left">
                    <img src="<?= $iconUrl; ?>"/>
                </div>
                <div class="single-related__item--right">
                    <span><?= $relatedLabel; ?>:</span>
                    <h5><?php the_title(); ?></h5>
                </div>
            </a>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
    </div>
<?php endif; ?>
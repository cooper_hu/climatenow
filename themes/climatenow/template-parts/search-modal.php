<div class="search-drawer">
    <div class="search-drawer-header" style="background-image: url(<?= get_field('search-bg', 'option'); ?>);">
        <div class="search-drawer-container">
            <form class="search-form">
                <a href="#" class="search-form--close-btn">
                    <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-close-2.svg"/>
                </a>
                <input type="text" placeholder="What are you looking for?"/>
            </form>
        </div>
    </div>
    <div class="search-drawer-body">
        <div class="search-drawer-container">
            <div class="search-drawer-loading-icon">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/spinner-icon.gif"/>
            </div>
            <div class="search-drawer-inner">
                <?php /*
                <?php $args = array( 
                    'post_type'     => array('video', 'podcast', 'article'), 
                    'posts_per_page' => -1,
                    's' => 'exosuit',
                    'meta_query' => array(
                        'relation' => 'OR',
                        array(
                          'key' => 'coming_soon',
                          'value' => '0',
                          'compare' => 'NOT EXISTS'
                        ),
                        array(
                          'key' => 'coming_soon',
                          'value' => '0',
                          'compare' => '=='
                        )
                    )
                );

                // The Query
                $the_query = new WP_Query(); 
                $the_query->parse_query( $args );
                relevanssi_do_query( $the_query );

                // The Loop
                if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post();  ?>
                       
                        <?php get_template_part( 'template-parts/search', 'modal-item' ); ?>
                       
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                */ ?>
            </div>
        </div>
    </div>
</div>
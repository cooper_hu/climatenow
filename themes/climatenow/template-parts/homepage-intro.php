<section class="hp-intro">
    <div class="hp-intro-vid js-hp-fadeout">
        <div class="hp-intro-vid--helper"></div>
        <video 
            class="hp-intro-vid__video"
            src="<?= get_template_directory_uri(); ?>/assets/videos/homepage.mov"  
            preload="none"
            role="video" 
            control="false"
            autoplay 
            loop 
            muted 
            playsinline
            ></video>
        <img class="hp-intro-vid__image js-ani-navlogo" src="<?= get_template_directory_uri(); ?>/assets/images/logo-cm-inverted.svg"/>
    </div>
    <div class="hp-intro-bottom">
        <?php if (get_field('headline')) : ?>
            <div class="hp-intro-bottom__left js-hp-fadeout">
                <h1 class="single-title"><?php the_field('headline'); ?></h1>
            </div>
        <?php endif; ?>
        <div class="hp-intro-bottom__right">
            
            <?php /*
            <h6 class="newsletter-form__headline tag"><?php the_field('footer-newsletter-headline', 'options'); ?></h6>
            
            <form action="<?php echo get_template_directory_uri(); ?>/inc/subscribe.php" method="post" class="newsletter-form">
                <div class="newsletter-form__input-wrapper">
                    <input class="newsletter-form__email" type="email" placeholder="Email Address"/>
                    <p class="error">*Please enter an email address</p>
                    <input type="submit" value="Sign Up"/>
                </div>
            </form>
            <br/><br/>
            */ ?>

            <div id="hpNewsletterHeadlineWrapper"><h6 class="newsletter-form__headline tag"><?php the_field('footer-newsletter-headline', 'options'); ?></h6></div>      

            <!--[if lte IE 8]>
            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
            <![endif]-->
            <div class="hp-hubspot">
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                  hbspt.forms.create({
                    region: "na1",
                    portalId: "21163325",
                    formId: "3e487390-ddb8-401b-97e2-7ef2ee1da361"
                });
                </script>
            </div>
            
        </div>
    </div>
</section>
<?php 
$iconUrl = get_template_directory_uri() . '/assets/icons/icon-podcast-black.svg';
$relatedLabel = 'Podcast';

$seriesInfo    = get_field('series');
$episodeNumber = get_field('episode');
$youtubeLink   = get_field('youtube');
$runtime       = get_field('runtime');
$contentGroup  = get_field('info');

$people = get_field('people');
$relatedMedia = get_field('related'); 

$section_id = 'podcast-' . get_field('series_number', $seriesInfo) . '-' . $episodeNumber; ?>

<section id="<?= $section_id; ?>">
    <a href="#" data-dest="#<?= $section_id; ?>" class="hp-features-item__scroll-link">Recent Podcast</a>
    <div class="col-smD-4">
        <div class="single-tag">Podcast Episode <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?></div>

        <h1 class="single-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
   
        <?php if ($contentGroup['subheadline']) : ?>
            <h2 class="single-title--sm"><?= $contentGroup['subheadline']; ?></h2>
        <?php endif; ?>
    </div>

    <div class="col-smD-flex">
        <div class="col-smD-3">
            <div class="single-description">
                <?= $contentGroup['description']; ?>
            </div>

            <div class="single-podcast-header-break small"></div>

            <?php get_template_part('template-parts/section-people'); ?>
        </div>
        <div class="col-smD-7">
            <div class="single-media-podcast__flex-wrapper">
                <div class="single-media-podcast__wrapper">
                    <div class="single-media-podcast">
                        <span class="single-media__date">Date: <?= get_the_date('m.d.Y'); ?></span>
                        <div class="single-media-podcast__inner">
                            <a href="<?php the_permalink(); ?>" class="single-media__cover podcast">
                                <?php $podcastImage = get_field('podcast_image'); ?>
                                <div class="single-media__cover--img" style="background-image: url(<?= $podcastImage['url']; ?>);"></div>
                                <div class="single-media__cover--text">
                                    <span class="single-podcast-btn">
                                        <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-podcast-white.svg"/>
                                        <span>Listen Now</span>
                                    </span>
                                </div>
                            </a>
                        </div>
                        <?php if ($runtime) : ?>
                            <span class="single-media__running-time">Running Time: <?= $runtime; ?> <span>min</span></span>
                        <?php endif; ?>
                    </div> 
                    
                </div>

                <div class="single-media-podcast__links">
                    <ul class="podcast-links">
                        <?php if (get_field('link_pocketcast')) : ?>
                            <li><a href="<?= get_field('link_pocketcast'); ?>" target="_blank" class="pocketcast"></a></li>
                        <?php endif; ?>
                        <?php if (get_field('link_apple')) : ?>
                            <li><a href="<?= get_field('link_apple'); ?>" target="_blank" class="apple"></a></li>
                        <?php endif; ?>
                        <?php if (get_field('link_google')) : ?>
                            <li><a href="<?= get_field('link_google'); ?>" target="_blank" class="google"></a></li>
                        <?php endif; ?>
                        <?php if (get_field('link_spotify')) : ?>
                            <li><a href="<?= get_field('link_spotify'); ?>" target="_blank" class="spotify"></a></li>
                        <?php endif; ?>
                        <?php if (get_field('link_stitcher')) : ?>
                            <li><a href="<?= get_field('link_stitcher'); ?>" target="_blank" class="stitcher"></a></li>
                        <?php endif; ?>
                        <?php if (get_field('link_youtube')) : ?>
                            <li><a href="<?= get_field('link_youtube'); ?>" target="_blank" class="youtube"></a></li>
                        <?php endif; ?>
                        <?php if (get_field('link_rss')) : ?>
                            <li><a href="<?= get_field('link_rss'); ?>" target="_blank" class="rss"></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>

            <div class="single-related-wrapper flex">
                <div class="host hide--mobile">
                    <?php get_template_part('template-parts/section-host'); ?>
                </div>
                <div class="related">
                    <?php get_template_part('template-parts/section-related-media'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
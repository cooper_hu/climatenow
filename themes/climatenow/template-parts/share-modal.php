<div class="mfp-hide share-popup" id="shareModal">
    <span title="Close (Esc)" type="button" class="share-popup__close-btn mfp-close">X</span>
    
    <h4 class="title--sm">Share <?= $post->post_type; ?>:</h4>
    <div class="share-popup__icon-wrapper">
        <?php // Twitter ?>
        <a class="share-popup__icon js-popup"
            href="https://twitter.com/intent/tweet/?url=<?= get_the_permalink(); ?>&text=<?= get_the_title(); ?>"
            target="_blank">
                <i class="fab fa-twitter"></i>
        </a>
        <?php // Facebook ?>
        <a class="share-popup__icon js-popup"
            href="http://www.facebook.com/sharer.php?u=<?= get_the_permalink(); ?>"
            target="_blank">
                <i class="fab fa-facebook-square"></i>
            </a>
        <?php // LinkedIn ?>
        <a class="share-popup__icon js-popup"
            href="http://www.linkedin.com/shareArticle?mini=true&url=<?= get_the_permalink(); ?>" 
            target="_blank">
                <i class="fab fa-linkedin"></i>
        </a>
        <?php // Email ?>
        <a class="share-popup__icon"
            href="mailto:?subject=Climate Now: <?= get_the_title(); ?>&amp;body=Check out this <?= $post->post_type; ?> <?= get_the_permalink(); ?>" 
            target="_blank">
                <i class="fas fa-envelope"></i>
        </a>
    </div>
</div>
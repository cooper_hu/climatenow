<?php
// $host   = get_the_terms($post->ID, 'host');
$host = get_field('host'); 

if ($host && $post->post_type != 'article') : ?>
    <div class="single-people">
        <h3 class="single-headline">Hosted By:</h3>
        <div class="single-people__flex">
        
            <?php foreach( $host as $term ): ?>
                <?php $image = get_field('image', $term); 
                      $bio   = get_field('bio', $term); ?>
                <?php if ($bio) : ?>
                    <a class="single-people__item hoverable" href="#host<?= $term->term_id; ?>">
                        <div class="single-people__item--img-wrapper">
                            <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                        </div>
                        <div class="single-people__item--content">
                            <h4 class="single-people__item--name"><?php echo esc_html( $term->name ); ?><br/><span><?= get_field('title', $term); ?></span></h4>
                        </div>
                    </a>

                    <div class="mfp-hide" id="host<?= $term->term_id; ?>">
                        <div class="person-popup">
                            <span title="Close (Esc)" type="button" class="person-popup__close-btn mfp-close">X</span>
                            <div class="person-popup__top">
                                <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                            </div>
                            <div class="person-popup__bottom">
                                <h4 class="single-title"><?php echo esc_html( $term->name ); ?></h4>
                                <h5 class="single-title--sm"><?= get_field('title', $term); ?></h5>

                                <div class="person-popup__bottom--flex">
                                    <div class="img">
                                        <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                                    </div>
                                    <div class="bio">
                                        <?= $bio; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="single-people__item">
                        <div class="single-people__item--img-wrapper">
                            <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                        </div>
                        <div class="single-people__item--content">
                            <h4 class="single-people__item--name"><?php echo esc_html( $term->name ); ?><br/><span><?= get_field('title', $term); ?></span></h4>
                        </div>
                    </div>
                <?php endif; ?>
                <?php $hostCount++; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
<?php 
$runtime = get_field('runtime'); ?>

<div class="single-media-podcast__flex-wrapper">
    <div class="single-media-podcast__wrapper">
        <div class="single-media-podcast">
            <span class="single-media__date">Date: <?= get_the_date('m.d.Y'); ?></span>
            <div class="single-media-podcast__inner">
                <a href="#" class="single-media__cover podcast single-page" data-title="<?php the_title(); ?>" data-mp3="<?= get_field('mp3'); ?>">
                   
                    <?php $podcastImage = get_field('podcast_image'); ?>
                    <div class="single-media__cover--img" style="background-image: url(<?= $podcastImage['url']; ?>);"></div>
                    <div class="single-media__cover--text">
                        <span class="single-podcast-btn">
                            <!--
                            <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-podcast-white.svg"/>
                        -->
                            <span>Listen Now</span>
                        </span>
                    </div>
                </a>
                <div class="single-media__podcast-wrapper">
                    <div id="single-song-player">
                      <div class="bottom-container">
                        <progress class="amplitude-song-played-progress" id="song-played-progress"></progress>
                        <div class="time-container">
                          <span class="current-time">
                            <span class="amplitude-current-minutes"></span>:<span class="amplitude-current-seconds"></span>
                          </span>
                          <span class="duration">
                            <span class="amplitude-duration-minutes"></span>:<span class="amplitude-duration-seconds"></span>
                          </span>
                        </div>
                        <div class="control-container">
                          <div class="amplitude-play-pause" id="play-pause"></div>
                          <div class="meta-container">
                            <span data-amplitude-song-info="name" class="song-name"></span>
                            <span data-amplitude-song-info="artist"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <?php if ($runtime) : ?>
                <span class="single-media__running-time">Running Time: <?= $runtime; ?> <span>min</span></span>
            <?php endif; ?>
        </div> 
    </div>

    <div class="single-media-podcast__links">
        <ul class="podcast-links">

            <?php if (get_field('link_amazon')) : ?>
                <li><a href="<?= get_field('link_amazon'); ?>" target="_blank" class="amazon"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_pocketcast')) : ?>
                <li><a href="<?= get_field('link_pocketcast'); ?>" target="_blank" class="pocketcast"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_apple')) : ?>
                <li><a href="<?= get_field('link_apple'); ?>" target="_blank" class="apple"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_google')) : ?>
                <li><a href="<?= get_field('link_google'); ?>" target="_blank" class="google"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_spotify')) : ?>
                <li><a href="<?= get_field('link_spotify'); ?>" target="_blank" class="spotify"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_stitcher')) : ?>
                <li><a href="<?= get_field('link_stitcher'); ?>" target="_blank" class="stitcher"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_youtube')) : ?>
                <li><a href="<?= get_field('link_youtube'); ?>" target="_blank" class="youtube"></a></li>
            <?php endif; ?>
            <?php if (get_field('link_rss')) : ?>
                <li><a href="<?= get_field('link_rss'); ?>" target="_blank" class="rss"></a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>

<?php 
$iconUrl = get_template_directory_uri() . '/assets/icons/icon-video-black.svg';
$relatedLabel = 'Video'; 

$seriesInfo    = get_field('series');
$episodeNumber = get_field('episode');
$youtubeLink   = get_field('youtube');
$contentGroup  = get_field('info');

$people = get_field('people');
$relatedMedia = get_field('related'); 

$section_id = 'video-' . get_field('series_number', $seriesInfo) . '-' . $episodeNumber; ?>

<section id="<?= $section_id; ?>">
    <a href="#" data-dest="#<?= $section_id; ?>" class="hp-features-item__scroll-link">Recent Episode</a>

    <div class="col-smD-4">
        <div class="single-tag">Video Episode <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?></div>

        <h1 class="single-title <?php if ($contentGroup['subheadline']) echo 'margin-bottom-sm'; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

        <?php if ($contentGroup['subheadline']) : ?>
            <h2 class="single-title--sm" style="margin-top: 0;"><?= $contentGroup['subheadline']; ?></h2>
        <?php endif; ?>
    </div>

    <div class="col-smD-flex">
        <div class="col-smD-3">
            <div class="single-description">
                <?= $contentGroup['description']; ?>
            </div>

            <div class="single-podcast-header-break small"></div>

            <?php get_template_part('template-parts/section-people'); ?>
        </div>
        <div class="col-smD-7">
            <?php get_template_part('template-parts/media-video'); ?>

            <div class="single-related-wrapper flex">
                <?php if( have_rows('transcript_flex') ) : ?>
                    <a href="<?php the_permalink();?>#transcript" class="single-headline transcript-btn hide--mobile">Read Transcript</a>
                <?php endif; ?>
                <div class="host">
                    <?php get_template_part('template-parts/section-host'); ?>
                </div>
                <div class="related">
                    <?php get_template_part('template-parts/section-related-media'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="subnav-drawer">
    <a href="#" class="subnav-drawer__close-btn">&nbsp;</a>
    <nav class="subnav-drawer__taxonomy-nav">
        <ul>
            <li><a href="#" data-type="Videos" class="" id="subnavCatVideos">Videos</a></li>
            <li><a href="#" data-type="Podcasts" class="" id="subnavCatPodcasts">Podcasts</a></li>
            <?php // check if article toggle is on ?>
            <?php if (get_field('hide-articles', 'options') != true) : ?>
                <li><a href="#" data-type="Articles" class="" id="subnavCatArticles">Articles</a></li>
            <?php endif; ?>
        </ul>
    </nav>

    <?php $videoTerms = get_terms( 'video-series' ); ?>

    <?php if ($videoTerms) : ?>
        <nav class="subnav-series" id="subnavSectionVideos">
            <?php foreach ( $videoTerms as $videoTerm ) :
                $videosQuery = new WP_Query( array(
                    'post_type' => 'video',
                    'posts_per_page' => -1,
                    'meta_key'  => 'episode',
                    'orderby'   => 'meta_value',
                    'order'     => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'video-series',
                            'field' => 'slug',
                            'terms' => array( $videoTerm->slug ),
                            'operator' => 'IN',
                        )
                    ),
                    'meta_query' => array(
                        // Hides coming soon items from query
                        'relation' => 'OR',
                        array(
                            'key' => 'coming_soon',
                            'value' => '1',
                            'compare' => 'NOT EXISTS'
                        ),
                        array(
                            'key' => 'coming_soon',
                            'value' => '0',
                            'compare' => '=='
                        )
                    )
                ) );

                $seriesNumber = get_field('series_number', $videoTerm);
                ?> 
                
                <?php if ( $videosQuery->have_posts() ) : ?>
                    <h6 class="ani-stagger-in">Series <?= $seriesNumber; ?></h6>
                        <ul>
                        <?php while ( $videosQuery->have_posts() ) : $videosQuery->the_post(); ?>
                            <?php $episodeNumber = get_field('episode'); ?>
                            <?php if (!get_field('coming_soon')) : ?>
                                <li class="ani-stagger-in">
                                    <a href="<?php the_permalink(); ?>">
                                        <span class="number"><?= $seriesNumber; ?>.<?= $episodeNumber; ?></span>
                                        <span class="title"><?php the_title(); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                
                <?php
                // Reset things, for good measure
                $videosQuery = null;
                wp_reset_postdata();
            endforeach; ?>
        </nav>
    <?php endif; ?>

    <?php $podcastTerms = get_terms( 'podcast-seasons' ); ?>
    <?php if ($podcastTerms) : ?>
        <nav class="subnav-series " id="subnavSectionPodcasts">
            <?php foreach ( $podcastTerms as $podcastTerm ) :
                $podcastQuery = new WP_Query( array(
                    'post_type' => 'podcast',
                    'posts_per_page' => -1,
                    'meta_key'  => 'episode',
                    'orderby'   => 'meta_value',
                    'order'     => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'podcast-seasons',
                            'field' => 'slug',
                            'terms' => array( $podcastTerm->slug ),
                            'operator' => 'IN'
                        )
                    ),
                    'meta_query' => array(
                        // Hides coming soon items from query
                        'relation' => 'OR',
                        array(
                            'key' => 'coming_soon',
                            'value' => '1',
                            'compare' => 'NOT EXISTS'
                        ),
                        array(
                            'key' => 'coming_soon',
                            'value' => '0',
                            'compare' => '=='
                        )
                    )
                ) );

                $seriesNumber = get_field('series_number', $podcastTerm);
                ?>
                
                <?php if ( $podcastQuery->have_posts() ) : ?>
                    <h6 class="ani-stagger-in">Season <?= $seriesNumber; ?></h6>
                    <ul>
                        <?php while ( $podcastQuery->have_posts() ) : $podcastQuery->the_post(); ?>
                            <?php $episodeNumber = get_field('episode'); ?>
                            <li class="ani-stagger-in">
                                <a href="<?php the_permalink(); ?>">
                                    <span class="number"><?= $seriesNumber; ?>.<?= $episodeNumber; ?></span>
                                    <span class="title"><?php the_title(); ?></span>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                
                <?php
                // Reset things, for good measure
                $podcastQuery = null;
                wp_reset_postdata();
            endforeach; ?>
        </nav>
    <?php endif; ?>

    <?php // NEED TO ADD ARTICLE LATCH ?>
    <?php $articleTerms = get_terms( 'article-series' ); ?>
    <?php if ($articleTerms) : ?>
        <nav class="subnav-series" id="subnavSectionArticles">
            <?php foreach ( $articleTerms as $articleTerm ) :
                $articleQuery = new WP_Query( array(
                    'post_type' => 'article',
                    'posts_per_page' => -1,
                    'meta_key'  => 'episode',
                    'orderby'   => 'meta_value',
                    'order'     => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'article-series',
                            'field' => 'slug',
                            'terms' => array( $articleTerm->slug ),
                            'operator' => 'IN',
                        )
                    ),
                    'meta_query' => array(
                        // Hides coming soon items from query
                        'relation' => 'OR',
                        array(
                            'key' => 'coming_soon',
                            'value' => '1',
                            'compare' => 'NOT EXISTS'
                        ),
                        array(
                            'key' => 'coming_soon',
                            'value' => '0',
                            'compare' => '=='
                        )
                    )
                ) );

                $seriesNumber = get_field('series_number', $articleTerm); ?> 
                
                <?php if ( $articleQuery->have_posts() ) : ?>
                    <h6 class="ani-stagger-in">Series <?= $seriesNumber; ?></h6>
                    <ul>
                        <?php while ( $articleQuery->have_posts() ) : $articleQuery->the_post(); ?>
                            <?php $episodeNumber = get_field('episode'); ?>
                            <li class="ani-stagger-in">
                                <a href="<?php the_permalink(); ?>">
                                    <span class="number"><?= $seriesNumber; ?>.<?= $episodeNumber; ?></span>
                                    <span class="title"><?php the_title(); ?></span>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                
                <?php
                // Reset things, for good measure
                $articleQuery = null;
                wp_reset_postdata();
            endforeach; ?>
        </nav>
    <?php endif; ?>
</div>
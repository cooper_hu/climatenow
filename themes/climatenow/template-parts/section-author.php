
<div class="single-people">
    <h3 class="single-headline">Written By:</h3>
    <div class="single-people__flex">
        <?php // Author
        $author_id   = get_the_author_meta( 'ID' );
        $author_name = get_the_author_meta( 'display_name', $author_id );
        $author_bio  = get_field('bio', 'user_' . $author_id);
        $image = get_field('image', 'user_' . $author_id);

        if ($author_bio) : ?>
            <a class="single-people__item hoverable" href="#author1">
                <div class="single-people__item--img-wrapper">
                    <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                </div>
                <div class="single-people__item--content">
                    <h4 class="single-people__item--name"><?= $author_name; ?><br/><span><?= get_field('title', 'user_' . $author_id); ?></span></h4>
                </div>
            </a>

            <div class="mfp-hide" id="author1">
                <div class="person-popup">
                    <span title="Close (Esc)" type="button" class="person-popup__close-btn mfp-close">X</span>
                    <div class="person-popup__top">
                        <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                    </div>
                    
                    <div class="person-popup__bottom">
                        <h4 class="single-title"><?= $author_name; ?></h4>
                        <h5 class="single-title--sm"><?= get_field('title', 'user_' . $author_id); ?></h5>
                        <div class="person-popup__bottom--flex">
                            <div class="img">
                                <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $image['alt']; ?>"/>
                            </div>
                            <div class="bio">
                                <?= $author_bio; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php else : ?>
            <div class="single-people__item">
                <div class="single-people__item--img-wrapper">
                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                </div>
                <div class="single-people__item--content">
                    <h4 class="single-people__item--name"><?= $author_name; ?><br/><span><?= get_field('title', 'user_' . $author_id); ?></span></h4>
                </div>
            </div>
        <?php endif; ?>
        
    </div>
</div>
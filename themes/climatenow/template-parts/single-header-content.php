<?php // Header specific field values
$seriesInfo    = get_field('series');
$episodeNumber = get_field('episode');
$runtime       = get_field('runtime');
$contentGroup  = get_field('info');  ?>

<div class="single-bg-header__content js-ani-fadeout">
    <div class="container--single">
        <div class="col-smD-4">
            <div class="single-tag js-ani-fadeout-trigger"><?= $post->post_type; ?> Episode <?php the_field('series_number', $seriesInfo); ?>.<?= $episodeNumber; ?>
                <a href="#shareModal" class="single-share">
                    <i class="fas fa-share-alt"></i>
                </a>
            </div>
            
            <h1 class="single-title"><?php the_title(); ?></h1>
            
            <?php if ($contentGroup['subheadline']) : ?>
                <h2 class="single-title--sm"><?= $contentGroup['subheadline']; ?></h2>
            <?php endif; ?>

            <div class="single-description">
                <?= $contentGroup['description']; ?>
                
                <?php if ($runtime) : ?>
                    <?php if ($post->post_type === 'video') : ?>
                        <?php if ($runtime  == 1) : ?>
                            <p class="no-margin">Running Time: <?= $runtime; ?> min</p>
                        <?php else : ?>
                            <p class="no-margin">Running Time: <?= $runtime; ?> mins</p>
                        <?php endif; ?>
                    <?php else : ?>
                        <p class="no-margin">Reading Time: <?= $runtime; ?> mins</p>
                    <?php endif; ?>
                <?php endif; ?>

                <p class="no-margin">Date: <?= get_the_date('m.d.Y'); ?></p>
            </div>
        </div>
    </div>
</div>
<?php 
$runtime      = get_field('runtime'); 
$bg_type      = get_field('header_type');
$header_image = get_field('header_image');
$header_video = get_field('header_video');
?>

<div class="single-media">
    <span class="single-media__date">Date: <?= get_the_date('m.d.Y'); ?></span>
    <div class="single-media__inner">
        <a href="<?php the_permalink(); ?>" class="single-media__cover">
            <?php if ($bg_type === 'image') : ?>
                <div class="single-media__cover--img" style="background-image: url(<?= $header_image['url']; ?>);"></div>
            <?php elseif ($bg_type === 'video') : ?>
                <video 
                    class="single-media__inner--video" 
                    src="<?= $header_video; ?>" 
                    preload="none"
                    role="video" 
                    control="false"
                    autoplay 
                    loop 
                    muted 
                    playsinline
                    ></video>
            <?php else : ?>
                <div class="single-media__cover--img" style="background-image: url(<?= get_the_post_thumbnail_url(); ?>);"></div>
            <?php endif; ?>
            <div class="single-media__cover--text">
                <span class="single-podcast-btn">
                    <img src="<?= get_template_directory_uri(); ?>/assets/icons/icon-video-white.svg"/>
                    <span>Watch Now</span>
                </span>
            </div>
        </a>
    </div>
    <?php if( have_rows('transcript_flex') ) : ?>
        <a href="<?php the_permalink();?>#transcript" class="single-headline single-media__transcript show--mobile">Read Transcript</a>
    <?php endif; ?>
    <?php if ($runtime) : ?>
        <span class="single-media__running-time">Running Time: <?= $runtime; ?> <span>min</span></span>
    <?php endif; ?>
</div> 
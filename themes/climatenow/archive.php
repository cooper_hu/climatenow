    <?php
/**
* The template for displaying archive pages
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package climatenow
*/

get_header();

if ($post->post_type === 'video') {
    $archiveLabel = 'Video';
    $archiveLabelExtra = 'Series';
    $archiveType = 'video';
    $archiveTax = 'video-series';
} else if ($post->post_type === 'podcast') {
    $archiveLabel = 'Podcast';
    $archiveLabelExtra = 'Seasons';
    $archiveType = 'podcast';
    $archiveTax = 'podcast-seasons';
} else {
    $archiveLabel = 'Article';
    $archiveLabelExtra = 'Series';
    $archiveType = 'article';
    $archiveTax = 'article-series';
}

?>

<main id="primary" class="single-page-wrapper <?php if ($archiveType === 'podcast') { echo 'podcast-archive'; } ?>" data-archive="<?= $archiveType; ?>">
    <?php if ($archiveType === 'podcast') : ?>
        <!--<div class="archive-header">
            <div class="container--archive">-->
                <?php $podcast_links = get_field('podcast_links', 'options'); ?>
                <div class="archive-header__podcast-links">
                    <ul class="podcast-links">
                        <?php if ($podcast_links['amazon']) : ?>
                            <li><a href="<?= $podcast_links['amazon']; ?>" target="_blank" class="amazon"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['pocketcast']) : ?>
                            <li><a href="<?= $podcast_links['pocketcast']; ?>" target="_blank" class="pocketcast"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['apple']) : ?>
                            <li><a href="<?= $podcast_links['apple']; ?>" target="_blank" class="apple"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['google']) : ?>
                            <li><a href="<?= $podcast_links['google']; ?>" target="_blank" class="google"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['spotify']) : ?>
                            <li><a href="<?= $podcast_links['spotify']; ?>" target="_blank" class="spotify"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['stitcher']) : ?>
                            <li><a href="<?= $podcast_links['stitcher']; ?>" target="_blank" class="stitcher"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['youtube']) : ?>
                            <li><a href="<?= $podcast_links['youtube']; ?>" target="_blank" class="youtube"></a></li>
                        <?php endif; ?>
                        <?php if ($podcast_links['rss']) : ?>
                            <li><a href="<?= $podcast_links['rss']; ?>" target="_blank" class="rss"></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
        <!--    </div>
        </div>-->
    <?php endif; ?>
        <?php if ( have_posts() ) : ?>

            <?php $terms = get_terms( $archiveTax ); ?>

            <?php if ($terms) : ?>
                
                <div class="archive-content-wrapper">
                    <?php foreach ( $terms as $term ) :
                        $termQuery = new WP_Query(array(
                            'post_type' => $archiveType,
                            'posts_per_page' => -1,
                            'meta_key'  => 'episode',
                            'meta_type' => 'NUMERIC',
                            'orderby'   => 'meta_value',
                            'order'     => 'DESC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => $archiveTax,
                                    'field' => 'slug',
                                    'terms' => array( $term->slug ),
                                    'operator' => 'IN',
                                )
                            )
                        ));

                        $seriesTitle  = get_field('series_title', $term);
                        $seriesNumber = get_field('series_number', $term); ?> 
                        <div class="archive-section new-header" id="<?= $term->slug; ?>">
                            
                            <div class="container--archive">
                                <div class="archive-section-intro">
                                    <h2 class="single-title--sm no-margin-bottom"><?php echo ($archiveType === 'podcast') ? 'Season' : 'Series'; ?> <?php echo ($seriesNumber > 10) ? $seriesNumber : '0'.$seriesNumber; ?></h2>
                                    <h3 class="single-title no-margin-top"><?= $seriesTitle; ?></h3>
                                    
                                    <?php if ($term->description) : ?>
                                        <p><?= $term->description; ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="container-archive-wrapper">
                                <?php if ( $termQuery->have_posts() ) : ?>
                                    <div class="container-archive-wrapper__flex">
                                        <?php while ( $termQuery->have_posts() ) : $termQuery->the_post(); ?>
                                            <?php $episodeNumber = get_field('episode'); 
                                                  $info = get_field('info');?>
                                            
                                            <?php if (get_field('coming_soon')) : ?>
                                                <div class="card animated">
                                                    <div class="card__inner coming-soon">
                                                        <div class="card__inner--img <?= $archiveType; ?> coming-soon">
                                                            <?php if (get_the_post_thumbnail_url()) : ?>
                                                                <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span class="single-tag label"><?= $archiveLabel; ?> Episode <?= $seriesNumber; ?>.<?= $episodeNumber; ?>
                                                            | <?= get_field('coming_soon_date'); ?>
                                                        </span>
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php if (!$info['hide-summary']) : ?>
                                                            <?php if ($info['summary']) : ?>
                                                                <p><?= $info['summary']; ?></p>
                                                            <?php else : ?>
                                                                <?php if ($info['description']) : ?>
                                                                    <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php else : ?>
                                                <div class="card animated">
                                                    <a href="<?php the_permalink(); ?>" class="card__inner">
                                                        <div class="card__inner--img <?= $archiveType; ?>">
                                                            <?php if (get_the_post_thumbnail_url()) : ?>
                                                                <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span class="single-tag label"><?= $archiveLabel; ?> Episode <?= $seriesNumber; ?>.<?= $episodeNumber; ?>
                                                            | <?= get_the_date( 'm.d.Y' ); ?>
                                                        </span>
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php if (!$info['hide-summary']) : ?>
                                                            <?php if ($info['summary']) : ?>
                                                                <p><?= $info['summary']; ?></p>
                                                            <?php else : ?>
                                                                <?php if ($info['description']) : ?>
                                                                    <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>


                            <?php /* 
                            <div class="container--archive--carousel">
                                <?php if ( $termQuery->have_posts() ) : ?>
                                    <div class="scrolling-wrapper">
                                        
                                        <?php while ( $termQuery->have_posts() ) : $termQuery->the_post(); ?>
                                            <?php $episodeNumber = get_field('episode'); 
                                                  $info = get_field('info');?>
                                            
                                            <?php if (get_field('coming_soon')) : ?>
                                                <div class="card animated">
                                                    <div class="card__inner coming-soon">
                                                        <div class="card__inner--img <?= $archiveType; ?> coming-soon">
                                                            <?php if (get_the_post_thumbnail_url()) : ?>
                                                                <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span class="single-tag label"><?= $archiveLabel; ?> Episode <?= $seriesNumber; ?>.<?= $episodeNumber; ?>
                                                            | <?= get_field('coming_soon_date'); ?>
                                                        </span>
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php if (!$info['hide-summary']) : ?>
                                                            <?php if ($info['summary']) : ?>
                                                                <p><?= $info['summary']; ?></p>
                                                            <?php else : ?>
                                                                <?php if ($info['description']) : ?>
                                                                    <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php else : ?>
                                                <div class="card animated">
                                                    <a href="<?php the_permalink(); ?>" class="card__inner">
                                                        <div class="card__inner--img <?= $archiveType; ?>">
                                                            <?php if (get_the_post_thumbnail_url()) : ?>
                                                                <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span class="single-tag label"><?= $archiveLabel; ?> Episode <?= $seriesNumber; ?>.<?= $episodeNumber; ?>
                                                            | <?= get_the_date( 'm.d.Y' ); ?>
                                                        </span>
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php if (!$info['hide-summary']) : ?>
                                                            <?php if ($info['summary']) : ?>
                                                                <p><?= $info['summary']; ?></p>
                                                            <?php else : ?>
                                                                <?php if ($info['description']) : ?>
                                                                    <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        <div class="card gutter"></div>
                                    </div>
                                </div>

                                <div class="archive-carousel-wrapper">
                                    <div class="archive-carousel">
                                        <div class="card first"></div>
                                        <?php while ( $termQuery->have_posts() ) : $termQuery->the_post(); ?>
                                            <?php $episodeNumber = get_field('episode'); 
                                                  $info = get_field('info'); ?>
                                            <?php if (get_field('coming_soon')) : ?>
                                                <div class="card animated">
                                                    <div class="card__inner coming-soon">
                                                        <div class="card__inner--img <?= $archiveType; ?> coming-soon">
                                                            <?php if (get_the_post_thumbnail_url()) : ?>
                                                                <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span class="single-tag label"><?= $archiveLabel; ?> Episode <?= $seriesNumber; ?>.<?= $episodeNumber; ?> | <?= get_field('coming_soon_date'); ?></span>
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php if (!$info['hide-summary']) : ?>
                                                            <?php if ($info['summary']) : ?>
                                                                <p><?= $info['summary']; ?></p>
                                                            <?php else : ?>
                                                                <?php if ($info['description']) : ?>
                                                                    <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php else : ?>
                                                <div class="card animated">
                                                    <a href="<?php the_permalink(); ?>" class="card__inner">
                                                        <div class="card__inner--img <?= $archiveType; ?>">
                                                            <?php if (get_the_post_thumbnail_url()) : ?>
                                                                <img src="<?= get_the_post_thumbnail_url(); ?>"/>
                                                            <?php endif; ?>
                                                        </div>
                                                        <span class="single-tag label"><?= $archiveLabel; ?> Episode <?= $seriesNumber; ?>.<?= $episodeNumber; ?>
                                                            | <?= get_the_date( 'm.d.Y' ); ?>
                                                        </span>
                                                        <h4><?php the_title(); ?></h4>
                                                        <?php if (!$info['hide-summary']) : ?>
                                                            <?php if ($info['summary']) : ?>
                                                                <p><?= $info['summary']; ?></p>
                                                            <?php else : ?>
                                                                <?php if ($info['description']) : ?>
                                                                    <p><?= substr(strip_tags($info['description']), 0, 210); ?>...</p>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>

                                            

                                        <?php endwhile; ?>
                                        
                                    </div>
                                </div>
                            <?php endif; ?>
                            */ ?>
                            
                        </div>
                        
                        <?php wp_reset_postdata();
                    endforeach; ?>
                </div>
               
            <?php endif; ?>
        <?php endif; ?>

</main><!-- #main -->

<?php
get_footer();
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package climatenow
 */

?>


	<footer id="colophon" class="site-footer">
		<div class="container--single">
            <div class="footer-top">
                <div class="footer-top__left">
                    <div class="footer-logo-mobile">
                        <a href="/"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-climate-now.svg"/></a>
                    </div>

                    <div class="footer-logo-desktop">
                        <?php $link = get_field('privacy-link', 'options'); ?>
                        <a href="/"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-climate-now.svg"/></a>
                        <p>&copy; <?php echo Date('Y'); ?> CLIMATE NOW. All rights reserved. 
                            <?php if( $link ) : ?>
                                <?php 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                                <a href="<?= $link_url; ?>" target="<?= $link_target; ?>"><?= $link_title; ?></a>
                            <?php endif; ?>
                        </p>
                    </div>
                    
                </div>
                <div class="footer-top__center">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'footer-menu',
                            'menu_class'     => 'footer-menu',
                        )
                    ); ?>
                </div>
                <div class="footer-top__right">
                    <div class="footer-form-wrapper">
                        <div class="footer-form-wrapper__inner">

                            <?php /* 
                            <h6 class="newsletter-form__headline"><?php the_field('footer-newsletter-headline', 'options'); ?></h6>
                            <form action="<?php echo get_template_directory_uri(); ?>/inc/subscribe.php" method="post" class="newsletter-form">
                                <div class="newsletter-form__input-wrapper">
                                    <input class="newsletter-form__email" type="email" placeholder="Email Address"/>
                                    <p class="error">*Please enter an email address</p>
                                    <input type="submit" value="Sign Up"/>
                                </div>
                            </form>
                            */ ?>

                            <div id="footerNewsletterHeadlineWrapper">
                                <h6 class="newsletter-form__headline"><?php the_field('footer-newsletter-headline', 'options'); ?></h6>
                            </div>

                            <!--[if lte IE 8]>
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                            <![endif]-->
                            <div class="footer-hubspot">
                                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                                <script>
                                  hbspt.forms.create({
                                    region: "na1",
                                    portalId: "21163325",
                                    formId: "72b9bc12-33a0-4eb4-81e3-8f45253e6850"
                                });
                                </script>
                            </div>

                            <!-- Begin Constant Contact Inline Form Code -->
                            <!--
                            <div id="footerNewsletter" class="ctct-inline-form" data-form-id="a79e0c78-c5a6-48fe-99da-91bcf0c30842"></div>
                        -->
                            <!-- End Constant Contact Inline Form Code -->
                        </div>

                        <?php $social = get_field('social_links', 'options'); 

                        if ($social) : ?>
                            <nav class="footer-social">
                                <?php if ($social['youtube']) : ?>
                                    <a href="<?= $social['youtube']; ?>" target="_blank">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                <?php endif; ?>

                                <?php if ($social['instagram']) : ?>
                                    <a href="<?= $social['instagram']; ?>" target="_blank">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                <?php endif; ?>

                                <?php if ($social['linkedin']) : ?>
                                    <a href="<?= $social['linkedin']; ?>" target="_blank">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                <?php endif; ?>

                                <?php if ($social['twitter']) : ?>
                                    <a href="<?= $social['twitter']; ?>" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                <?php endif; ?>
                            </nav>
                        <?php endif; ?>
                    </div>
                    
                </div>
		    </div>

            <div class="footer-bottom">
                <div class="footer-bottom__right hide-desktop">
                    <p>&copy; <?php echo Date('Y'); ?> CLIMATE NOW. All rights reserved. <a href="/privacy">Privacy Policy</a></p>
                </div>
            </div>
        </div>
	</footer>
</div><!-- ./site-wrapper -->
<?php wp_footer(); ?>



<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/21163325.js"></script>
<!-- End of HubSpot Embed Code -->

<script>
    // document.getElementById("email_address_0").placeholder = "Email Address";
</script>

<?php $is_prod = stripos($_SERVER['HTTP_HOST'],"climatenow.com") !== false;
if($is_prod) : ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NH37WKM"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php endif; ?>

</body>
</html>

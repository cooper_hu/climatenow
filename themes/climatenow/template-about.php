<?php
/**
 * Template Name: About
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package climatenow
 */

get_header();

$about_tag = get_field('tag');
$about_headline = get_field('headline');
$about_text = get_field('text');
?>

<main id="primary" class="single-page-wrapper">
    <div class="container--single">
        <div class="col-smD-4">

            <header class="about-header">
                <?php if ($about_tag) : ?>
                    <span class="single-tag"><?= $about_tag; ?></span>
                <?php endif; ?>


                
                <h1 class="single-title"><?= $about_headline; ?></h1>
                    
                <?= $about_text; ?>
            </header>
        </div>
    </div>

    <?php if( have_rows('group') ): ?>
        <?php while( have_rows('group') ): the_row(); 
            $group_title = get_sub_field('group_title');
            $team_members = get_sub_field('team_members') ?>
            <section class="about-section">
                <div class="container--single">
                    <span class="single-tag fixed"><?= $group_title; ?></span>

                    <?php if( have_rows('team_members') ): ?>
                        <div class="about-team-section">
                            <?php while( have_rows('team_members') ) : the_row(); ?>
                                <?php $image = get_sub_field('image');
                                      $name = get_sub_field('name');
                                      $about = get_sub_field('about'); ?>
                                <div class="team-member">
                                    <img src="<?= $image['sizes']['portrait']; ?>" alt="<?= $alt['url']; ?>"/>
                                    <h3><?= $name; ?></h3>
                                    <?= $about; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php
get_footer();

// Keep Data object outside of function so it can be used by load more.
var data = {},
    timer,
    x;

$('.nav-bar__search').on('click', function(e) {
    e.preventDefault();
});

$(".search-form").on('submit', function(e) {
    e.preventDefault();
});

$(".search-form input").keyup(function () {
    if (!$(this).val()) {
      $(".search-form--close-btn").fadeOut();
    } else {
      $(".search-form--close-btn").fadeIn();

      $('.search-drawer-inner').html('');
        $('.search-drawer-loading-icon').addClass('visible');

        var searchedTerm = $(this).val();

        if (x) { x.abort() } // If there is an existing XHR, abort it.
        clearTimeout(timer); // Clear the timer so we don't end up with dupes.
        timer = setTimeout(function() { // assign timer a new timeout 
            x = searchSite(searchedTerm);
        }, 2000); 

    }
});

$(".search-form--close-btn").on('click', function(e) {
    $(".search-form input").val('');
    $(".search-form--close-btn").fadeOut();
    $('.search-drawer-inner').html('');
});

//user is "finished typing," do something
function searchSite (searchterm) {
    data = {
        action : "propertyfilter",
        search: searchterm,
    };  

    // console.log('Searched Obj:', data);

    $.ajax({
        url: wp_ajax.ajax_url,
        data : data,
        type: 'post',
        success: function(results) {
            $('.search-drawer-loading-icon').removeClass('visible');
            if (results === 'NOPOST') {
                $('.search-drawer-inner').html('<p class="no-results">No Results... Try another search.</p>');
            } else {
                
                $('.search-drawer-inner').html(results);
            }
        },
        error: function(results) {
            console.warn(results);
            $('.properties__load-more--indicator.all-properties').removeClass('visible');
        }
    });
   
}



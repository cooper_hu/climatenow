<?php
/**
 * Template Name: Homepage
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package climatenow
 */

get_header();
?>

<main id="primary" class="single-page-wrapper">
    <div class="hp-intro-wrapper">
        <?php if (get_field('show_trailer')) : ?>
            <a href="<?= get_field('youtube', false, false); ?>" class="hp-intro-trailer">
                <span class="play-btn"></span>
                <span class="text">Watch our Trailer</span>
            </a>
        <?php endif; ?>

        <?php if (get_field('show-webby', 'options')) : ?>
            <a class="webby-badge homepage" href="https://winners.webbyawards.com/2022/video/video-series-channels/science-education/206146/climate-now" target="_blank">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/webby-badge-2.png"/>
            </a>
        <?php endif; ?>

        <div class="container--single">
            <?php get_template_part( 'template-parts/homepage', 'intro' ); ?>
        </div>
    </div>

    <div class="hp-features" id="hpContent">
            <?php // Featured Content

            $featured_content = get_field('featured_content');
            
            if ($featured_content) : ?>
                
                <?php foreach( $featured_content as $post ): 
                    setup_postdata($post); 
                    $type = $post->post_type ?>
                    <div class="hp-features-item">
                        <div class="container--single">
                            <?php if ($type === 'video') {
                                get_template_part( 'template-parts/card', 'video' );
                            } elseif ($type === 'podcast') {
                                get_template_part( 'template-parts/card', 'podcast' );
                            } else {
                                get_template_part( 'template-parts/card', 'article' );
                            } ?>
                        </div>
                    </div>
                    
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
                
            <?php endif; ?>
            
    </div>
    <div class="hp-footer <?php if (count($featured_content) % 2 == 0) { echo 'white'; } ?>">
        <div class="container--single">
            <a href="<?php the_field('all-content-page','options'); ?>" class="btn accent--left animate">See All Media</a>
        </div>
    </div>
    
</main><!-- #main -->

<?php
get_footer();
